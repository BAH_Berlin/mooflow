# Always prefer setuptools over distutils
from setuptools import setup, find_packages
import pathlib

here = pathlib.Path(__file__).parent.resolve()

long_description = (here / 'README.md').read_text(encoding='utf-8')

setup(
    name="mooflow", # Replace with your own username
    version="0.7.b1",
    author="Ruben Müller, Büro für Angewandte Hydrologie",
    author_email="software@bah-berlin.de",
    description="""Mooflow: A generalized framework for basin-scale 
multi-objective simulation-optimization with MODFLOW""",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/BAH_Berlin/base_bah",
    packages=find_packages(where='mooflow'), 
    package_dir={'': 'mooflow'}, 
    license = "gpl",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: gpl",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.5, <4',
    setup_requires=["numpydoc"],
    install_requires=["pywr", "pandas", "flopy", "deap", "numpy", "wheel"],
)