# -*- coding: utf-8 -*-
"""
Created on Wed Apr 26 10:32:37 2017

@author: user
"""


def make_shape_two(item, date=None, length=2, filler='0'):
    '''returns a string with a given length. padding with 0.
    -- if item is a datetime-object, then date must be given.
    -- if item is a int or str, then date is None

    Parameters
    ----------
    item : datetime-object or str / int
        the str to be padded
    date : string
        unit (month, year, day, minute, second)
    length : int
        length of output string'''
    if date:    
        if date == 'month':
            item_str = str(item.month)
        elif date == 'year':
            item_str = str(item.year)
        elif date =='day':
            item_str = str(item.day)
        elif date == 'hour':
            item_str = str(item.hour)
        elif date == 'minute':
            item_str = str(item.minute)
        elif date == 'second':
            item_str = str(item.second)
    else:
        item_str = str(item)
    zeros = filler
    diff = length-len(item_str)
    if diff > 0:
        for i in range(0, diff-1):
            zeros = zeros + filler
        item_str = zeros + item_str
    return item_str
