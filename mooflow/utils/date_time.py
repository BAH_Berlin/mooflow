# -*- coding: utf-8 -*-
"""
Functions for dates and stuff.

@author: Ruben Müller
@email: ruben.mueller@bah-berlin.de
"""
import numbers
import collections
from datetime import datetime
from datetime import timedelta
from dateutil import relativedelta


def is_leap_year(year):
    """ if year is a leap year return True
        else return False """
    if year % 100 == 0:
        return year % 400 == 0
    return year % 4 == 0


def doy(Y, M=1, D=1):
    """ given year, month, day return day of year
        Astronomical Algorithms, Jean Meeus, 2d ed, 1998, chap 7 """
    if isinstance(Y, numbers.Real):
        if is_leap_year(Y):
            K = 1
        else:
            K = 2
        N = int((275 * M) / 9.0) - K * int((M + 9) / 12.0) + D - 30
        return N
    else:
        return Y.toordinal() - datetime(Y.year, 1, 1).toordinal() + 1 


def ymd(Y, N=None):
    """ given year = Y and day of year = N, return year, month, day
        Astronomical Algorithms, Jean Meeus, 2d ed, 1998, chap 7 """  
        
    if isinstance(Y, datetime):
        N = doy(Y)
        Y = Y.year
        
        
    if is_leap_year(Y):
        K = 1
    else:
        K = 2
    M = int((9 * (K + N)) / 275.0 + 0.98)
    if N < 32:
        M = 1
    D = N - int((275 * M) / 9.0) + K * int((M + 9) / 12.0) + 30
    return Y, M, D


def datetime_ymd(dt):
    '''convert [doy, year] into datetime.datetime'''
    if not isinstance(dt, list):
        raise TypeError('dt must be [doy, year]')
    return datetime(*ymd(*dt[::-1]))


def dy_datetime(dt):
    if not isinstance(dt, datetime):
        raise TypeError('dt must be datetime.datetime object')
    return doy(dt), dt.year

def woy(time_curr, year=None):
    """ given the current datetime or doy and year, lookup the week of the year"""
    if isinstance(time_curr, int) and isinstance(year, int):
        time_curr = datetime(*ymd(year, time_curr))
    elif isinstance(time_curr, datetime):
        pass
    else:
        raise TypeError('time_curr must be either datetime or both time_curr (day of year) and year must be int.')
    return ((time_curr - datetime(time_curr.year, 1, 1)).days // 7) + 1
    
            
def full_steps_timediff(d1, d2, settingtime=0):
    '''calculate the timesteps in weeks, months and days between 2 dates.
    
    Parameters
    ----------
    d1 : datetime or iterable
        the starting date as datetime or as [doy, year]
    d2 : datetime or iterable
        the ending date as datetime or as [doy, year]
    
    Returns
    -------
    dict : dict
        the number of timesteps for month (key 12), week (key 7) and days (key 1)
    '''
    if not isinstance(d2, datetime):
        d2 = datetime_ymd(d2)
    if not isinstance(d1, datetime):
        d1 = datetime_ymd(d1)
    
    if settingtime:
        d1 += timedelta(days=settingtime -1)
    
    #d1 -= timedelta(days=1)
    
    #This will find the difference between the two dates
    difference = relativedelta.relativedelta(d2, d1)
    
    full_months = difference.years * 12 + difference.months 
    if difference.days >29:
        full_months += 1 
    #full_weeks = full_months * 4 + difference.weeks + 1 #+ difference.days//7
    full_days = (d2 - d1).days -1
    full_weeks = full_days // 7
    return {12: full_months, 7: full_weeks, 1: full_days}    


def timestep_gen(cls):
    '''yields a timestep
    
    Parameters
    ----------
    starting_date : list
        consists of two int [day of year, year]
    timesteps : int
        the number of time steps in the simulation
    timestep : int
        the temporary resolution. 1: day, 7: week, 12: month
        
    Returns
    -------
    tmp : list
        consists of [timestep counter, [current day of year, current year]]
    '''
    if cls.setting_time:
       tmp = cls.setting_time[1].copy() 
    else:
        tmp = cls.starting_date.copy()
    if cls.timestep == 1:
        eoy = 365 + is_leap_year(tmp[1])
    elif cls.timestep == 7:
        eoy = 52
    else:
        eoy = 12
    for i in range(cls.timesteps[cls.timestep]):
        if i == cls.timesteps[cls.timestep]-1:
            tmpx = cls.setting_time[1] if cls.setting_time else cls.starting_date
        tmpx = (i, tmp)
        #print("  ", tmpx)
        yield tmpx
        
        if tmp[0] >= eoy:
            tmp[0] = 1
            tmp[1] += 1
        else:
            tmp[0] += 1 


def check_iter(lst, critical=False):
    '''check if the argument is iterable
    
    Parameters
    ----------
    lst : object
        check if this is iterable
    critical : bool
        raise an error or simple return False
        
    Returns
    -------
    bool : is it iterable?'''
    if not isinstance(lst, collections.Iterable):
        if critical:
            raise TypeError('lst is not iterable')
        else:
            return False
    return True


def check_l2(lst, critical=False):
    '''check if a list consists of sublists that are iterable
    
    Parameters
    ----------
    lst : object
        check if this is iterable
    critical : bool
        raise an error or simple return False
        
    Returns
    -------
    bool : is it iterable?'''
    check = False
    if check_iter(lst, critical=critical):
        check = True
        for slist in lst:
            if not check_iter(slist):
                check=False
                break
    return check
    

def days_dt(dt):
    '''calculate the number of days for any month
    
    Parameters
    ----------
    dt : list, datetime.datetime
        list with (year, dayOfYear) or datetime.datetime object to look up
        
    Returns
    -------
    int : the number of days'''
    if isinstance(dt, collections.Iterable) and len(dt) == 2:
        dt = datetime_ymd(dt)
    return (dt.replace(month = dt.month % 12 +1, day = 1) - timedelta(days=1)).day



def days_in_month(dt):
    '''calculate the number of days for any month for individual (year, dayOfYear),
    datetime.datetime or lists of these 
    
    Parameters
    ----------
    dt : list, datetime.datetime
        list with (year, dayOfYear) or datetime.datetime object to look up
        
    Returns
    -------
    int of list of int: the number of days'''    
    if isinstance(dt, datetime.datetime):
        return days_dt(dt)
    elif isinstance(dt, collections.Iterable):
        if isinstance(dt[0], (int, float)):
            return days_dt(dt)
        else:
            days_month = []
            for slst in dt:
                days_month.append(days_dt(slst))  
        return days_month
    
    
def stressperiod_day(**kwargs):
    '''returns a list with the number of days for each stress period.
    A stress period is a month. The length of the returned list is 
    the number of total stress periods in the simulation.
    
    Parameters
    ----------
    **kwargs : ...
        'datetime' : list with datettime of starttime and simulation length in days
        
        'doy': list with list(year, doy) and simulation length in days
        
        'setup': omo.classes.Omo_setup object
    
    Returns
    -------
    list with the number of days for each stress period (month) and, if extended,
        plus the beginning of month'''
        
    if 'datetime' in kwargs:
        sttime = kwargs['datetime'][0]
        timesteps = kwargs['datetime'][1]
    elif 'doy' in kwargs:
        sttime = datetime.datetime(*ymd(*kwargs['doy'][0])) 
        timesteps = kwargs['doy'][1]
    elif 'setup' in kwargs:
        timesteps = kwargs['setup'].ending_date - kwargs['setup'].starting_date
        sttime = kwargs['setup']
        
    if 'extended' in kwargs:
        extended = kwargs['extended']
    else:
        extended = False

        
    stressperiods = []
    total_ts = 0
    protector = 0
    while total_ts <= timesteps and protector < 2400:
        if total_ts == 0:
            ndays = full_steps_timediff(sttime,
                sttime.replace(day=days_dt(sttime)))[1]
            if extended:
                stressperiods.append([ndays, sttime])
            else:
                stressperiods.append(ndays)
            total_ts += ndays
            c_date = sttime.replace(day=1) + relativedelta.relativedelta(months=1)
        else:
            ndays = days_dt(c_date)
            total_ts += ndays
            if total_ts > timesteps:
                ndays -= total_ts-timesteps
            if extended:
                stressperiods.append([ndays, c_date])
            else:
                stressperiods.append(ndays)
            c_date += relativedelta.relativedelta(months=1)
        protector += 1

    return stressperiods  

if __name__ == "__main__":
    pass