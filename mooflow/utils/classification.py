# -*- coding: utf-8 -*-
"""
Created on Wed Apr 26 10:31:48 2017

@author: user
"""
import bisect 
import pandas as pd 


def class_by_bisect(value, ranges, direction='left'): 
    ''' 
    bisect an list for a value and return the position 
     
    Parameters 
    ---------- 
    value : int or float 
        value to look-up 
    ranges : list 
        look-up list needs to be sorted 
         
    Returns 
    ------- 
    position : int 
        the right side position''' 
    if isinstance(value, pd.core.series.Series): 
        value = value.iloc[0] 
    if direction == 'left':
        position = bisect.bisect_left(ranges, value)
    else:
        position = bisect.bisect_right(ranges, value)
    return position 
