# -*- coding: utf-8 -*-
"""
Functions for optimization parameter handling.

@author: Ruben Müller
@email: ruben.mueller@bah-berlin.de
"""

import collections
import numpy as np
from mooflow.classes import Extr_pattern


def check_boundary_len(boundary, pattern_a):
    '''check the length of a boundary list against an pattern.

    Parameters
    ----------
    boundary: float or Iterable
        the upper or lower boundary
    pattern_a : Extr_pattern object
        The extraction pattern

    Returns
    -------
    boundary : list
        the upper or lower boundary. If an Int was provided, return a list
        with the appropiate length'''
        
    #if not boundary or not pattern_a:
     #   return boundary
    
    ### check if pattern is a list of lists with times and extractions
    if isinstance(pattern_a, Extr_pattern):
        len_p = len(pattern_a[0])
    elif isinstance(pattern_a, int):
        len_p = pattern_a
    else:
        ValueError('pattern_a must be int or Extr_pattern object')

    if isinstance(boundary, collections.Iterable):
        len_b = max(np.shape(boundary))
        if len_b == 1 and len_b < len_p:
            boundary = [boundary[0] for x in range(len_p)]
    else:
        boundary = [boundary for x in range(len_p)]

    if not max(np.shape(boundary)) == len_p:
        raise ValueError('the provided boundary list has not the same length as the pattern: {} to {}'.format(len(boundary), len_p))
    return boundary


def check_lu_range(bound_l, bound_u):
    '''raise an ValueError if a value in the lower boundary list is higher then
    in the upper boundary list'''
    if any([x[0] > x[1] for x in zip(bound_l, bound_u)]):
        raise ValueError('lower bound is (partially) higher then the upper bound. {} - {}'.format(bound_l, bound_u))


def recalc_parameter(parameter, bound_l, bound_u):
    '''Rescale the parameter list from [0,..., 1] into the range
    given by the lower and upper boundary lists.

    Parameters
    ----------
    parameter : int or list
        the scaled parameter [0,..., 1]
    bound_l : Iterable
        the lower boundary
    bound_u : Iterable
        the upper boundary

    Returns
    -------
    parameter : list
        the rescaled parameter (multiplicator)'''
    parameter = [parameter] if not isinstance(parameter, collections.Iterable) else parameter
    
    if not bound_l or not bound_u:
        raise ValueError('No valid boundaries.')
        
    len_p = len(bound_l)
    
    if isinstance(bound_l, collections.Iterable):
        len_b = len(parameter)
        if len_b == 1 and len_b < len_p:
            parameter = [parameter[0] for x in range(len_p)]
    else:
        parameter = [parameter for x in range(len_p)]

    if not len(parameter) == len_p:
        raise ValueError('the provided parameter list has not the same length as the pattern: {} to {}'.format(len(parameter), len_p))

    return [parameter[x[0]] * (x[1][1] - x[1][0]) + x[1][0] for x in enumerate(zip(bound_l, bound_u))]




if __name__ == "__main__":
    pattern_a = Extr_pattern([45, 123, 223, 300, 365], [1, 2, 3, 4, 1])
    para = [0.5, 0.5, 0.5, 0.5, 0.5]

    bound_l = [1.5, 1.7, 2, 2, 1.7]
    bound_u =  [0.7, 1.3, 1.3, 1.3, 0.8]

    bound_u = check_boundary_len(bound_u, pattern_a)
    bound_l = check_boundary_len(bound_l, pattern_a)

    check_lu_range(bound_l, bound_u)

    p2 = recalc_parameter(para, bound_l, bound_u)


if __name__ == "__main__":
    pass
