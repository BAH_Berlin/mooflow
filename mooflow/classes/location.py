# -*- coding: utf-8 -*-
"""
Ground water level sensitive areas.

@author: Ruben Müller
@email: ruben.mueller@bah-berlin.de
"""

import numbers
import numpy as np
import collections
from mooflow.classes import GW_level_pattern

#%%

__LOC_TYPE  = ('area head ecology', 'area head evaporation', 'groundwater_level', 'intrusion_protection')

class Area_gw(GW_level_pattern):
    
    def __init__(self, idx, area_list, area_list_y=None, correct_zero=None):
        '''Class for a ground water level sensitive area.
        
        Parameters
        ----------
        idx : str
            the identification of the area
        area_list : list
            a list of int respresenting the column ids
            or a list of lists with [column and row ids]
        area_list_y : list
            needed if area_list is list with column ids
        correct_zero : int
            add x correct_zero to the column and row ids'''
        self.id = idx
        self.sector = None
        self.z = 0
        self.weights = 1
        self.__cfz = correct_zero
        self.area = self.__set_area(area_list, area_list_y)
        self.fitness_group = 'undefined'
        
    ################ area 
    @property
    def area(self):
        return self.__area
    
    
    @area.setter
    def area(self, area_list, area_list_y=None):
        self.__area = self.__set_area(area_list, area_list_y)
    
    
    ################ z (layer) 
    @property
    def z(self):
        return self.__z
    
    
    @z.setter

    def z(self, z_value):
        if not isinstance(z_value, collections.Iterable):
            self.__z = [int(z_value)]
        else:
            self.__z = z_value
    

    ################ weights (layer) 
    @property
    def weights(self):
        return self.__weights
    
    
    @weights.setter
    def weights(self, w_value):
        """weight different z levels diffrently"""
        if not isinstance(w_value, collections.Iterable):
            w_value = [w_value]
  
        if not len(w_value) == len(self.z):
            raise ValueError('Lengths of weights and layers (z) are different. weights: {} vs z: {}'.format(len(w_value), len(self.z)))
            
        self.__weights = w_value 


    ################ type
    @property
    def type(self):
        return self.__type


    @type.setter
    def type(self, loc_type):
        if not loc_type.lower() in __LOC_TYPE:
            raise ValueError('Supported types of wells: {}'.format(__LOC_TYPE))
        self.__type = loc_type.lower()


    ################ sector 
    @property
    def sector(self):
        return self.__sector
    
    
    @sector.setter
    def sector(self, for_sector):
        """Assign the location to a sector"""
        self.__sector = for_sector
        

    ################ fitness_group  
    @property
    def fitness_group(self):
        return self.__fitness_group


    @fitness_group.setter
    def fitness_group(self, group):
        """Assign to a fitness_group"""
        if not isinstance(group, str):
            raise TypeError('fitness_group must be a string')
        self.__fitness_group = group
        
    ##################
        
  
    def apply_area_function1(self, x):
        """Apply the function that is assigned to the __ares_function."""
        res = self.__area_function(x)
        return res
    
    
    def set_area_function1(self, fargs, *args, **kwargs):
        '''Set a function that evaluated the numpy array that is read from the
        head file.
        
        Parameters
        ----------
        fargs : function
            the function that is applied
            *args and **kwargs are passed the the function fargs
        
        Notes
        -----
        You will build this 
            >>>  lambda x: fargs(x, *args, **kwargs)
        and apply it by calling the method apply_area_function
        
        See also
        --------
        apply_area_function
        '''
        self.__area_function = lambda x: fargs(x, *args, **kwargs)
 

    def __set_area(self, area_list, area_list_y):
        '''set the area'''
        if isinstance(area_list[0], numbers.Real) and not area_list_y:
            raise ValueError('if area_list is a list of integers, provide area_list_y')
        
        if isinstance(area_list[0], int) and isinstance(area_list[1], int):
            area = [area_list, area_list_y]
            
        ### upper left and lower right
        elif len(area_list) == 2 and area_list_y == 'ullr':
            x = [1, 0] if area_list[0][0] > area_list[1][0] else [0, 1]
            
            area = [list(range(area_list[x[0]][0], area_list[x[1]][0])), 
                           list(range(area_list[0][1], area_list[1][1]))]
        elif len(area_list) > 1:
            area = area_list
        else:
            raise IndexError('area must be a list greater then [[x0, y0], [x1, y1]]')

        if self.__cfz:
            area = [(x[0] + self.__cfz, x[1] + self.__cfz) for x in area]
        return area


    def area_np_array(self):
        '''returns the x and y coordinates of the area as a numpy array'''
        return np.array(list(zip(self.__area[0], self.__area[1])))

 
    ################ protocoll

          
    def __repr__(self):
        return 'AreaGW(%r, %r, %r, %r)' % (self.id, self.area, None, self.__cfz)
    
    
    def __str__(self):
        return 'AreaGW with {} locations {}... and z values {}'.format(len(self.area), self.area[:4], self.z)
    


if __name__ == "__main__":
    if 0:
        area51 = Area_gw(34443, [[300, 400], [400, 500]], correct_zero=2)
        print(area51)
        area51
