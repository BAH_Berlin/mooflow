#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Registry for wells, well_groups and ground water level sensitive areas.

@author: Ruben Müller
@email: ruben.mueller@bah-berlin.de
"""
from mooflow.classes import Well
from mooflow.classes import Wellgroup
from mooflow.classes import Area_gw

class Register:
    def __init__(self, reg_type):
        """Registry for wells, well_groups or ground water level sensitive areas.
        
        Parameters
        ----------
        reg_type: str
        well, well_group or area_gw"""
        self.__rt = reg_type
        self.registry = []
        if reg_type.lower() == 'well':
            self.rtype = Well
        elif reg_type.lower() ==  'wellgroup':
            self.rtype = Wellgroup
        elif reg_type.lower() ==  'area_gw':
            self.rtype = Area_gw
        else:
            raise TypeError('Registry for wrong type {}'.format(type(reg_type)))

    def add(self, obj):
        '''add an object to the register -> only if its ID is not already in the registry
        
        Parameters
        ----------
        obj : Well, Wellgroup or Area_gw from mooflow.classes'''
        check = any({w.id == obj.id for w in self.registry})
        if not check:
            self.registry.append(obj)
            
    
    def find_well_loc(self, loc):
        '''find the index of a well, defined by it's coordinates, in the Registry
        
        Parameters
        ----------
        loc : list, tuple
            the location with coordinates [z, y, x]'''
        if not self.rtype is Well:
            raise Exception('this method is only applicable for well - Registers')
        check = [all([well.location["z"]==loc[0],
                      well.location["y"]==loc[2],
                      well.location["x"]==loc[1]]) for well in self.registry]
        try:
            idx = check.index(True)
        except ValueError:
            idx = None
        return idx


    def discard(self, obj):
        '''dicard on object from the Registry
        
        Parameters
        ----------
        obj : Well, Wellgroup or Area_gw from mooflow.classes'''
        if isinstance(obj, self.rtype):
            x = [w.id == obj.id for w in self.registry]
        else:
            x = [w.id == obj for w in self.registry]
        if any(x):
            idx = x.index(True)
            self.registry.remove(idx)
        else:
            print('Well not found!')

    #################################################
    ### Protocol
    #################################################
    
    def __iadd__(self, obj):
        #print('iadd')
        if isinstance(obj, self.rtype):
            self.add(obj)
            return self
        else:
            raise TypeError('obj is not of type {} but of type {}'.format(self.rtype, type(obj)))


    def __getitem__(self, index):
        if isinstance(index, slice):
            return self.registry[index]
        elif isinstance(index, int):
            return self.registry[index]
        elif isinstance(index, str):
            try:
                idx = [i for i, w in enumerate(self.registry) if w.id == index][0]
            except:
                raise ValueError('Entry with id {} not found in registry'.format(index))
            return self.registry[idx]

    def __len(self):
        return len(self.registry)


    def __isub__(self, obj):
        if isinstance(obj, self.rtype):
            self.discard(obj)
            return self
        else:
            raise TypeError('obj is not of type {} but'.format(type(obj)))


    def __str__(self):
        return 'Registry for {} with {} entries: {}'.format(self.__rt, len(self.registry), self.registry)


    def __repr__(self):
        return 'Register(%r)' % self.rtype


    # TODO: implement
    def entries_in_rectangle(self, ul_lr):
        raise NotImplementedError


if __name__ == "__main__":
    pass

