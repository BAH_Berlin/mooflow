#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
The well class.

@author: Ruben Müller
@email: ruben.mueller@bah-berlin.de
"""

from mooflow.utils import doy
from datetime import datetime
from mooflow.classes.extraction_pattern import Extr_pattern


class Well(Extr_pattern):
    def __init__(self, idx, times, rates, multiplier=None):
        '''The class Well describes a well.
        
        Parameters
        ----------
        idx : str
            the identificator
        times : list
            list with the date of year in which a saison for the extraction pattern ends
            e.g. [45, 123, 223, 300, 365] for five saisons
        rates : list
            list with the same length as ext_times gives the extraction rates
        multiplier : list
            list with the same length as ext_times, sets multiplier for the exttraction rates
        '''
        super().__init__(times, rates, multiplier=None)
        self.type = 'extraction'
        ### the well ID, must be unique
        self.id = idx

        # for documentation
        self.info = 'Standard well'
        # sector --> for assignment into a fitness function
        self.sector = None
        # well is active soonest on this date
        self.active_on = [1, 1900]
        # location of the well
        self.location = {'x': None, 'y': None, 'z': None}
        ### the ID of the gallery, if the well belongs to one
        # is a property
        self.well_group = None
        # the pywr node that the extraction is given to
        self.pywr_node = None

        ### TODO: set other well IDs into this list, if we have an multilayer recharge well
        # is a property
        self.multilayer_recharge = []

    ################ location
    @property
    def location(self):
        return self.__location


    @location.setter
    def location(self, inp):
        """set the location of the well with dict inp.
        
        {x:, y: z:}, or {east, north, layer} or {rw, hw, z}
        """
        self.__location = {'x': None, 'y': None, 'z': None}
        if not isinstance(inp, dict):
            raise ValueError('Parameter must be a dictionary')
        for key in inp:
            if key.lower() in ('y', 'north', 'hw'):
                self.__location['y'] = inp[key]
            elif key.lower() in ('x', 'east', 'rw'):
                self.__location['x'] = inp[key]
            elif key.lower() in ('z', 'layer'):
                self.__location['z'] = inp[key]
            else:
                raise KeyError("""'Unsupported key ['y', 'north', 'hw';
                                                    'x', 'east', 'rw' or
                                                    'z', 'layer']""")


    ################ active_on
    @property
    def active_on(self):
        return self.__active_on

    
    @active_on.setter
    def active_on(self, active):
        """Date on which a well goes into production.
        
        Parameters
        ----------
        active : list or datetime.datetime
            for list use [day of year, year]"""
        self.__active_on = [doy(active), active.year] if isinstance(active, datetime) else active

    ################ active_on
    @property
    def pywr_node(self):
        return self.__pywr_node

    @pywr_node.setter
    def pywr_node(self, pywr_node=None):
        """map the well to this active node of the pywr model"""
        if pywr_node:
            if not isinstance(pywr_node, str):
                raise TypeError('Please provide an str as node ID')
        self.__pywr_node = pywr_node


    ################ type
    @property
    def type(self):
        return self.__type


    @type.setter
    def type(self, well_type):
        __WELL_TYPE = ('extraction', 'infiltration')
        if not well_type.lower() in __WELL_TYPE:
            raise ValueError('Supported types of wells: {}'.format(__WELL_TYPE))
        self.__type = well_type.lower()


    ################ sector
    @property
    def sector(self):
        return self.__sector


    ### TODO: pywr_nodes can not handle the distribution to multiple nodes
    @sector.setter
    def sector(self, for_sector):
        """not implemented yet. --> """
        self.__sector = for_sector


    def is_active_on_date(self, day, year):
        '''returns True if the date is after the well became active

        Parameters
        ----------
        year : int
            the year of the date
        day : int
            the day of the year (doy)'''
        is_active = True
        if self.active_on:
            if self.active_on[1] * 100000 + self.active_on[0] > year * 100000 + day:
                is_active = False
        return is_active


    ################ protocoll
    def __str__(self):
        return 'Well with ID {}'.format(self.id)


    def __repr__(self):
        return 'Well(%r, %r, %r, %r)' % (self.id, self.get_pattern_ext()[0], self.get_pattern_ext()[1], self.multiplier)




if __name__ == "__main__":
    if 0:
        w = Well('w1',[100, 200, 300, 365], [20, 50, 70, 15])
        w.set_pattern([100, 200, 300, 365], [10, 30, 30, 15])
        w.location = {'x': 466, 'y': 477, 'z': 2}
        w.get_extr_sum_a()
