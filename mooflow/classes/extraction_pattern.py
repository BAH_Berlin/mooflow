#!/usr/bin/env python3


# -*- coding: utf-8 -*-

"""
Extraction/Injection or demand patterns.

@author: Ruben Müller
@email: ruben.mueller@bah-berlin.de
"""
import bisect
import numbers

from datetime import datetime
from mooflow.utils import woy
from mooflow.utils import is_leap_year
from mooflow.utils import doy


class Extr_pattern:
    '''The class Pattern describes the inter annual pattern of extraction rate
    demands for a well.
    
    Parameters
    ----------
    ext_times : list
        list with the date of year in which a saison for the extraction pattern ends
        e.g. [45, 123, 223, 300, 365] for five saisons
    ext_rates : list
        list with the same length as ext_times gives the extraction rates
    multiplier : list
        list with the same length as ext_times, sets multiplier for the exttraction rates
    '''

    def __init__(self, ext_times, ext_rates=None, multiplier=None):
        self.__ext_times = [365]
        self.__ext_rates = [0]
        self.__dem_times = None
        self.__dem_rates = None

        self.__multiplier = multiplier if multiplier else [1 for i in ext_times]
        self.set_pattern(ext_times, ext_rates)

    ################ multiplier 
    @property
    def multiplier(self):
        '''property for the multiplier. The multiplier is for the actual extaction demand.'''
        return self.__multiplier


    @multiplier.setter
    def multiplier(self, multiplier):
        '''setter for the multiplier. The multiplier is for the actual extaction demand.

        Parameters
        ----------
        multiplier : float or iterable
            a list with the same length as periods or an single float'''
        if isinstance(multiplier, numbers.Real):
            if multiplier < 0:
                raise ValueError('negative multiplier not allowed.')
            multiplier = [multiplier for x in range(self.__ext_rates)]
        else:
            if any([m < 0 for m in multiplier]):
                raise ValueError('negative multiplier not allowed.')

        if not len(multiplier) == len(self.__ext_times):
            raise ValueError('multiplier and rates have not the same length multiplier: {} - b: {}'.format(len(multiplier), len(self.__rates)))
        self.__multiplier = multiplier
    ################
    

    def get_pattern_ext(self):
        return self.__ext_times, self.__ext_rates


    def get_pattern_dem(self):
        return self.__dem_times, self.__dem_rates


    def get_rate_ext(self, time_curr, year=None):
        '''Return the required extraction rate for the current day of year. Use this,
        if the pattern is a extraction pattern.

        Parameters
        ----------
        time_curr : int or datetime.datetime
            the day of year or date of the current time step
            if the day of year is provided as an integer, the year is required
            because we need to check for leap years
        year : int
            the year of the current time step

        Returns
        -------
            the current extration rate demand'''
        rate = self.__calc_rate_dem(time_curr, year)
        return rate
    
    
    def get_rate_dem(self, time_curr, year=None):
        '''Return the demand for the current day of year. Use this,
        if the pattern is a demand pattern.

        Parameters
        ----------
        time_curr : int or datetime.datetime
            the day of year or date of the current time step
            if the day of year is provided as an integer, the year is required
            because we need to check for leap years
        year : int
            the year of the current time step

        Returns
        -------
            the current extration rate demand'''
        rate = self.__calc_rate_dem(time_curr, year, demand=True)
        return rate
    
    
    
    def __calc_rate_dem(self, time_curr, year, demand=False):
        '''Return the extraction rate demnad for the current day of year

        Parameters
        ----------
        time_curr : int or datetime.datetime
            the day of year or date of the current time step
            if the day of year is provided as an integer, the year is required
            because we need to check for leap years
        year : int
            the year of the current time step

        Returns
        -------
            the current extration rate demand'''
        if demand:
            if not self.__dem_times and not self.__dem_rates:
                times = self.__ext_times
                rates = self.__ext_rates   
            else:
                times = self.__dem_times
                rates = self.__dem_rates
        else:
            times = self.__ext_times
            rates = self.__ext_rates            
            
        if isinstance(time_curr, numbers.Real) and not year:
                raise ValueError('provide a year if time_curr is day of year')

        if self.timestep == 1:
            if isinstance(time_curr, int):
                ### if we have an leap year, we subtract one day after the 28.Feb
                mn = 1 if is_leap_year(year) and time_curr > 59 else 0
                time_curr -= mn
            elif isinstance(time_curr, datetime):
                ttmp = time_curr.toordinal() - datetime(time_curr.year, 1, 1).toordinal() + 1
                mn = 1 if is_leap_year(time_curr.year) and doy(time_curr) > 59 else 0   
                time_curr = ttmp - mn

        elif self.timestep == 7 and isinstance(time_curr, datetime):
            time_curr = woy(time_curr)
        elif self.timestep == 12 and isinstance(time_curr, datetime):
            time_curr = time_curr.month

        #print(time_curr)
        position = bisect.bisect_left(times, min(time_curr, 365))

        rate = rates[position]

        if not demand:
            rate *= self.multiplier[position]

        return rate
    
    
    def get_sum_year(self, year=1901, demand=False):
        """Calculate annual sum of extractions/demands for a year"""
        days = 367 if is_leap_year(year) else 366
        if demand:
            sumy = sum([self.get_rate_dem(i, year) for i in range(1, days)])
        else:
            sumy = sum([self.get_rate_ext(i, year) for i in range(1, days)])
        return sumy
    

    def set_pattern(self, times, rates, demand=False):
        '''Set the pattern of required extraction rates and actual demands.

        Parameters
        ----------
        times : list of int
            a list with end of period dates. The pattern beginns at the first
            day of year/week/month.
            For day of year, the first entry is usually not 1, because we need
            *end of period values* (for datetimes e.g. datetime.datetime(1900, 7, 31)).
            The last entry needs to be 365 or 366 for day of year, 52 for weeks
            or 12 for months.
        rates : list of float
            a list of extraction rate demands with the same length as the
            time list
        demand : bool
            if True, set a demand pattern. Do this, if the actual necessary demand
            at a site is lower then the required extraction rate (f.e., if you allocate the surplus,
            which is extracted rate - demand)

        Note
        ----
        for annual constant rates provide two lists with length one: times=[365] and rate=[value]'''

        if not len(times) == len(rates):
            raise ValueError('a and b have not the same length --- times: {} - rates: {}'.format(len(times),
                                                                                                 len(rates)))
        if not times[-1] in (365, 366, 12, 52):
            raise ValueError('''times is a vector with end of period dates.
                             The last entry must relate to day 365 or 366 for days,
                             52 for weeks or 12 for months.
                             Got {}'''.format(times[-1]))

        if not demand:
            self.__ext_times = times
            self.__ext_rates = rates
            if times[-1] in (365, 366):
                self.timestep = 1 # timedelta(days=1)
            elif times[-1] == 52:
                self.timestep = 7 # timedelta(days=7)
            else:
                self.timestep = 12 # timedelta(months=1)
        else:
            self.__dem_times = times
            self.__dem_rates = rates


    def __str__(self):
        if self.timestep == 1:
            tstep = 'day'
        elif self.timestep == 52:
            tstep = 'week'
        else:
            tstep = 'month'
        txt = 'Pattern with ext_times {}, ext_rates {} and multiplier {} at timestep {}.'.format(self.__ext_times,
                                                                                                 self.__ext_rates,
                                                                                                 self.__multiplier,
                                                                                                 tstep)
        if self.__dem_times:
            txt += 'Plus Pattern has a demand with dem_times {} and dem_rates {}'.format(self.__dem_times,
                                                                                         self.__dem_rates)
        return txt


    def __repr__(self):
        return 'Pattern(%r, %r, %r)' % (self.__ext_times, self.__ext_rates, self.__multiplier)



#%%

if __name__ == "__main__":
    if 0:
        pattern_a = Extr_pattern([45, 123, 223, 300, 365], [1, 2, 3, 4, 5])
        print(pattern_a)
        pattern_a.set_pattern([365], [7], demand=True)
        print(pattern_a)
        print(pattern_a.get_rate_dem(123, 1943))
        print(pattern_a.get_rate_dem(datetime(2012, 1, 1)))
        print(pattern_a.get_rate_ext(123, 1943))
        print('extr year', pattern_a.get_sum_year())
        print('dem year', pattern_a.get_sum_year(demand=True))
        
        pattern_a = Extr_pattern([45, 123, 223, 300, 365], [1, 2, 3, 4, 5])
        print(pattern_a)
        print(pattern_a.get_rate_dem(123, 1943))
        print(pattern_a.get_rate_ext(123, 1943))
        print('extr year', pattern_a.get_sum_year())
        print('dem year', pattern_a.get_sum_year(demand=True))

