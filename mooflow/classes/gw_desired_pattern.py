#!/usr/bin/env python3


# -*- coding: utf-8 -*-

"""
Threshold patterns for ground water level sensitive areas.

@author: Ruben Müller
@email: ruben.mueller@bah-berlin.de
"""
import bisect
import numbers

from datetime import datetime
from mooflow.utils import woy
from mooflow.utils import is_leap_year



class GW_level_pattern:
    '''The class Pattern describes the inter annual pattern of extraction rate
    demands for a well.
    
    Parameters
    ----------
    level_times : list
        list with the date of year in which a saison for the threshold ends
        e.g. [45, 123, 223, 300, 365] for five saisons
    level_gw : list
        list with the same length as ext_times gives the threshold
    multiplier : list
        list with the same length as ext_times, sets multiplier for the exttraction rates'''

    def __init__(self, level_times, level_gw=None, multiplier=None):
        self.__level_times = [365]
        self.__level_gw = [0]

        self.set_pattern(level_times, level_gw)
    

    def get_levels(self):
        return self.__level_times, self.__level_gw


    def get_level(self, time_curr, year=None):
        '''Return the desired ground water level (head) for the current day of year. 

        Parameters
        ----------
        time_curr : int or datetime.datetime
            the day of year or date of the current time step
            if the day of year is provided as an integer, the year is required
            because we need to check for leap years
        year : int
            the year of the current time step

        Returns
        -------
            the current desired ground water level'''
        level = self.__calc_level(time_curr, year)
        return level
    

    
    def __calc_level(self, time_curr, year):
        '''Return the extraction rate demnad for the current day of year

        Parameters
        ----------
        time_curr : int or datetime.datetime
            the day of year or date of the current time step
            if the day of year is provided as an integer, the year is required
            because we need to check for leap years
        year : int
            the year of the current time step

        Returns
        -------
            the current extration rate demand'''

        times = self.__level_times
        levels = self.__level_gw
     
            
        if isinstance(time_curr, numbers.Real) and not year:
                raise ValueError('provide a year if time_curr is day of year')

        if self.timestep == 1:
            if isinstance(time_curr, int):
                ### if we have an leap year, we subtract one day after the 28.Feb
                #if is_leap_year(year):
                #    print('leapyear', year)
                mn = 1 if is_leap_year(year) and time_curr > 59 else 0
                time_curr -= mn
            elif isinstance(time_curr, datetime):
                time_curr = time_curr.toordinal() - datetime(time_curr.year, 1, 1).toordinal() + 1
                
        elif self.timestep == 7 and isinstance(time_curr, datetime):
            time_curr = woy(time_curr)
        elif self.timestep == 12 and isinstance(time_curr, datetime):
            time_curr = time_curr.month

        position = bisect.bisect_left(times, time_curr)
        level = levels[position]

        return level
    

    def set_pattern(self, times, rates):
        '''Set the pattern of required extraction rates and actual demands

        Parameters
        ----------
        times : list of int
            a list with end of period dates. The pattern beginns at the first
            day of year/week/month.
            For day of year, the first entry is usually not 1, because we need
            *end of period values* (for datetimes e.g. datetime.datetime(1900, 7, 31)).
            The last entry needs to be 365 or 366 for day of year, 52 for weeks
            or 12 for months.
        rates : list of float
            a list of extraction rate demands with the same length as the
            time list
        demand : bool
            if True, set a demand pattern. Do this, if the actual necessary demand
            at a site is lower then the required extraction rate (f.e., if you allocate the surplus,
            which is extracted rate - demand)

        Note
        ----
        for annual constant rates provide two lists with length one: times=[365] and rate=[value]'''

        if not len(times) == len(rates):
            raise ValueError('a and b have not the same length --- times: {} - rates: {}'.format(len(times),
                                                                                             len(rates)))
        if not times[-1] in (365, 366, 12, 52):
            raise ValueError('''times is a vector with end of period dates.
                             The last entry must relate to day 365 or 366 for days,
                             52 for weeks or 12 for months.
                             Got {}.'''.format(self.__level_times[-1]))

        self.__level_times = times
        self.__level_gw = rates
        if times[-1] in (365, 366):
            self.timestep = 1 # timedelta(days=1)
        elif times[-1] == 52:
            self.timestep = 7 # timedelta(days=7)
        else:
            self.timestep = 12 # timedelta(months=1)



    def __str__(self):
        if self.timestep == 1:
            tstep = 'day'
        elif self.timestep == 52:
            tstep = 'week'
        else:
            tstep = 'month'
        txt = 'Pattern with level_times {}, level_gw {} at timestep {}.'.format(self.__level_times,
                                                                                self.__level_gw,
                                                                                tstep)
        return txt


    def __repr__(self):
        return 'Pattern(%r, %r)' % (self.__level_times, self.__level_gw)



#%%

if __name__ == "__main__":
    if 0:
        pattern_a = GW_level_pattern([15, 46, 227, 305, 365], [1, 2, 3, 4, 5])
        print(pattern_a)
        pattern_b = GW_level_pattern([365], [5])
        print(pattern_b)
        pattern_b.set_pattern([365], [7])
        print(pattern_b)

