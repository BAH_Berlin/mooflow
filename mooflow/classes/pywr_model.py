# -*- coding: utf-8 -*-
"""
Manage the Pywr model.

@author: Ruben Müller
@email: ruben.mueller@bah-berlin.de
"""

import json
import os
import datetime
import numpy as np
try:
    from pywr.core import Model
except:
    class Modelx:
        def load(self, x):
            pass
    Model = Modelx()
from mooflow.classes import Omo_setup
from mooflow.read import lstfile_nwt_shortage
from pywr.recorders import TablesRecorder

def check_well(well, dday):
     check = False
     if well.is_active_on_date(dday.day, dday.year) and well.pywr_node:
         check = True
     return check


def dem_extr_balance(well, str_per):
    '''simple approach -- assume, that there are no deficits in the actual extraction'''
    bal = - well.get_rate_ext(str_per) + well.get_rate_dem(str_per)
    return bal


def sdemand(well, str_per):
    dem = well.get_rate_dem(str_per) * -1
    return dem

def sextraction(well, str_per):
    '''simple approach -- get the extraction demand'''
    dem = well.get_rate_ext(str_per) * -1
    return dem

def inp_node(node):
    return '<Input "' + node + '">'


class Pywr_model:
    def __init__(self, omo_setup, timeseries=False):
        '''Class to manage the water allocation with the Pywr package

        Parameters
        ----------
        omo_setup : mooflow.classes.Omo_setup
            the configured setup
        timeseries : Bool
            if False, make a simple balance model (one timestep)
            if True, take the number of timesteps from the omo_setup'''

        self.model = None
        self.__model_json = None
        self._mgmt_nodes = {}
        self._mgmt_nodes_i = {}
        self.recfile = None
        self.__usepwr = False
        self.recorder = None
        self.__threadnumber = omo_setup.threadnumber
        self.__timesteps = omo_setup.number_timesteps
        self.__modePywr = omo_setup.modePywr
        if not isinstance(omo_setup, Omo_setup):
            TypeError('Wrong type of omp_setup.')

        self.__path = omo_setup.modelfolder
        

    ################ active model nodes
    @property
    def active_nodes(self):
        return self.__nodes


    @active_nodes.setter
    def active_nodes(self, nodes=None):
        """Set the active nodes.
        
        Parameters
        ----------
        nodes : list
            a list with the active node names."""
        if not self.__model_json:
            raise ValueError('The JSON file is not read. Use load_json et al. first.')

        if not isinstance(nodes, (list, tuple, None)):
            raise TypeError('nodes must be a tuple or list with nodes or None')

        self.__modelkeys_ordered = [node['name'] for node in self.__model_json['nodes']]
        check = all([node in self.__modelkeys_ordered for node in nodes])

        if check or not nodes:
            self.__nodes = nodes
        else:
            print('Node given which is not in the model.', nodes)
            self.__nodes = nodes
            #raise ValueError('Node given which is not in the model.')


    ################ active model nodes
    @property
    def limit_zero(self):
        return self.__lim_zero


    @limit_zero.setter
    def limit_zero(self, nodes=None):
        """Active nodes as input nodes need positive values in the input time series.
        
        Parameters
        ----------
        nodes : list
            same length as active_nodes. 
            Values:
            for time series : set the lower bound for active nodes.
            0/False... active node can have negative flows at each timestep
            1/True ... active node is lower bound limited to 0
            -1     ... do not write the time series for the node at all"""
        if not self.__model_json:
            raise ValueError('The JSON file is not read. Use load_json et al. first.')
            
        if not self.__nodes:
            raise ValueError('No active nodes set yet. Use the actives_nodes attribute.')

        if not isinstance(nodes, (list, tuple, None)):
            raise TypeError('nodes must be a tuple or list with nodes or None')

        if not len(nodes) == len(self.active_nodes):
            raise ValueError("Mismatch in length for limit_zero and active_nodes.", len(nodes), len(self.active_nodes))

        self.__lim_zero = nodes



    ################ active model nodes
    @property
    def usePywr(self):
        return self.__usepywr


    @usePywr.setter
    def usePywr(self, usepywr=False):
        """Use a Pywr model or not."""
        if not isinstance(usepywr, bool):
            raise TypeError('usePywr must be bool, not {}'.format(type(usepywr)))
        self.__usepwr = usepywr


    def get_pywrfile(self):
        '''return the file name of the JSON model file'''
        return self.__pywrfile

    def get_json(self):
        '''return the dict of the pywr model'''
        return self.__model_json

    def get_active_nodes(self):
        '''return the active nodes'''
        return self.__nodes


    def set_pywrfile(self, file_name, datafile=None):
        '''Set the JSON model file for Pywr

        Parameters
        ----------
        file_name : str
            name of the JSON model file
        path : str, optional
            set another path to the model file then the default modflow model path'''

        if not file_name[-4:].lower() == 'json':
            raise ValueError('File needs to be a JSON file.')

        if os.path.dirname(file_name):
            self.__path = os.path.dirname(file_name)
        if os.path.dirname(datafile):
            self.__path_data = os.path.dirname(datafile)

        file_name = os.path.basename(file_name)
        datafile = os.path.basename(datafile)
        
        self.__pywrfile = os.path.join(self.__path, file_name)
        self.__datafile = os.path.join(self.__path, datafile)
        
        if not os.path.isfile(self.__pywrfile):
            raise FileNotFoundError('File {} not found'.format(self.__pywrfile))
       
        
        with open(self.__pywrfile, 'r') as fid:
            model_json = json.load(fid)
            tmp = self.__datafile
            
            self.__datafile = self.__datafile[:-4] + "_{}".format(self.__threadnumber) + ".csv"
            
            print(tmp, os.path.basename(tmp), self.__datafile, self.__threadnumber)
            if self.__threadnumber and not self.__modePywr == "total":
                
                if isinstance(model_json["parameters"], list):
                    for i, _ in enumerate(model_json["parameters"]):
                        if not "url" in model_json["parameters"]: continue
                        if model_json["parameters"][i]["url"] \
                        in (tmp, os.path.basename(tmp)):
                            model_json["parameters"][i]["url"] = self.__datafile
                else:
                    for i in model_json["parameters"]:
                        if not "url" in model_json["parameters"][i]: continue
                        if model_json["parameters"][i]["url"] \
                        in (tmp, os.path.basename(tmp)):
                            model_json["parameters"][i]["url"] = self.__datafile
                    
    
            self.__model_json = model_json


    def run_pywr(self, use_TablesRecorder=True):
        '''Start the water allocation optimization pywr.'''
        if not self.__model_json:
            raise ValueError('The model is not set up. Use load_json (and further functions to adopt the model). first.')
        self.model = Model.load(self.__model_json)
        
        if use_TablesRecorder:
            self.recfile = self.__pywrfile[:-5] + "_{}".format(self.__threadnumber) + ".h5" 
            if os.path.isfile(self.recfile):
                os.remove(self.recfile)
            self.recorder = TablesRecorder(self.model, self.recfile, parameters=[p for p in self.model.parameters])
        stats = self.model.run()
        return stats


    def prepare_pywr_model(self, setup_mf, reg_wells, reg_wellgroup, dict_node_shrt=None, calculate='sbalance'):
        '''Return a dictionary which summarizes for the active nodes.
        These dictionaries are needed to configure the INFLOW nodes with the
        deliverable amount of water, e.g.
        from the actual extracted water from well (groups).

        Parameters
        ----------
        setup_mf : mooflow.classes.Omo_setup
            the modflow setup
        reg_wells : mooflow.classes.Registry for wells
            the registry for the wells
        reg_wellgroup : mooflow.classes.Registry for well groups
            the registry for the well groups
        calculate : str, optional
            *sbalance*... simple (overall) balance demand - extraction for the active nodes;
            *sdemand*... simple demand for the active nodes;
            *sextraction*... simple extractions for the actice nodes

        Returns
        -------
        dict : a dictionary with an analysis regardig *calculate* for each '''
        ### build dictionary for nodes
        if dict_node_shrt is None:
            dict_input = {}
            [dict_input.update({key: np.zeros(self.timesteps)}) for key in self.active_nodes]
        else:
            dict_input = dict_node_shrt

        if calculate == 'sbalance':
            func = dem_extr_balance
        elif calculate == 'sdemand':
            func = sdemand
        elif calculate == 'sextraction':
            func = sextraction
        else:
            raise ValueError('function {} is not supported -- sbalance, sdemand, sextraction.'.format(calculate))

        if setup_mf.timesteps == 1:
            for i, date in enumerate(setup_mf.timesteps_enddates): 
                if i < setup_mf.setting_time:
                    continue
                ### loop over all wells
                for well in reg_wells:
                    ### look for wells wich are not in well_group
                    #if not well.well_group:
                    if not well.pywr_node in dict_input:
                        continue
                    if check_well(well, date):
                        bal =  func(well, date)
                        dict_input[well.pywr_node] += bal
        else:
            for i, date in enumerate(setup_mf.timesteps_enddates[setup_mf.setting_time:]):
                ### loop over all wells
                for well in reg_wells:
                    ### look for wells wich are not in well_group
                    #if not well.well_group:
                    if not well.pywr_node in dict_input:
                        continue
                    if check_well(well, date):
                        bal =  func(well, date)
                        dict_input[well.pywr_node][i] += bal
                            
                ### check for boundaries
                for n, node in enumerate(self.__nodes):
                    if self.__lim_zero[n]: 
                        dict_input[node][i] = max(dict_input[node][i], 0) if dict_input[node][i] < 0 else dict_input[node][i]

        return dict_input


    def update_nodes_in_model(self, factor=1):
        '''Configure the INFLOW nodes with the
        deliverable amount of water, e.g.
        from the actual extracted water from well (groups).'''
        if self.__timesteps == 1:
            for node in self.dict_node_ext:
                idx = self.__modelkeys_ordered.index(node)
                self.__model_json['nodes'][idx]['max_flow'] = abs(self.dict_node_ext[node][0]) * factor
        else:
            raise NotImplementedError('Allocation for time series not implemented yet.')


    def read_results(self, func=None, return_dict=True):
        '''Read the results from a pywr model run.

        Parameters
        ----------
        return_dict : bool, optional
            if True, return the results, otherwisestore them as an attribute.

        Returns
        -------
        dict : with overviews for each active node.'''
        
        self.jfile = self.get_json()
        
        if self.recorder is None:
            self.df = self.model.to_dataframe()
        else:
            df_gen = self.recorder.generate_dataframes(self.recfile)
            self.df = {}
            for name, dfc in df_gen:
                self.df.update({name: dfc})            

        results = func(self)
            
        if return_dict:
            return results
        else: 
            self.__results = results
      
    def pywt_extr_nwt(self, setup_mf, reg_well, reg_wellgroup):
        '''For active nodes:
        this function first accumulates the total needed extractions
        and then accumulates the total shortages in extractions which is
        read from the list file from the NWT model.
        --> the result is the total deliverable extraction for active nodes
    
        Parameters
        ----------
        setup_mf : mooflow.classes.Omo_setup
            the modflow setup
        reg_well : mooflow.classes.Registry for wells
            the registry for wells
        pywr_model : mooflow.classes.Pywr_model
            the Pywr setup
    
        Returns
        -------
        dict : the shortages for each well from the list file
        '''
        dict_node_shrt = self.nwt_well_shortages(setup_mf, reg_well)
        dict_node_ext = self.prepare_pywr_model(setup_mf, reg_well,
            reg_wellgroup, dict_node_shrt, 'sextraction')
        
        self.dict_node_ext = dict_node_ext


    def write_nodefile(self, setup_mf, delimiter=",", correct_day=None):
        """Write the CSV data file with all the well data for pywr.
        
        Parameters
        ----------
        setup_mf : mooflow.classes.Omo_setup
            the modflow setup
        delimiter : str
            the delimiter to use
        correct_day : int or None
        """
        if self.__modePywr == "monthly" and correct_day:
            datestr = "%Y-%m-{}".format(correct_day)
        else:
            datestr = "%Y-%m-%d"
        
        if self.__datafile is not None:
            with open(self.__datafile, "w") as fid:
                header = [x for x in self.dict_node_ext if self.__lim_zero[self.__nodes.index(x)] > -1]
                header.insert(0, "Date")
                fid.write(delimiter.join(header) + "\n")
                for i, date in enumerate(setup_mf.timesteps_enddates[setup_mf.setting_time:]):
                    cdate = datetime.datetime.strftime(date, datestr)
                    
                    data = delimiter.join([str(self.dict_node_ext[x][i]) for x in header[1:]])
                    fid.write(cdate + delimiter + data + "\n")


    def nwt_well_shortages(self, setup_mf, reg_well):
        '''read the shortages for each well from the list file for the NWT model for
        each time step for pywr active nodes.
    
        Parameters
        ----------
        setup_mf : mooflow.classes.Omo_setup
            the modflow setup
        reg_well : mooflow.classes.Registry for wells
            the registry for wells
        pywr_model : mooflow.classes.Pywr_model
            the Pywr setup
    
        Returns
        -------
        dict : the shortages for each well from the list file
        '''
    
        ### since there are no wells active in the steady state phase,
        ### we need not care about it while reading the shortages!
        if setup_mf.modePywr == "total":
            dict_act_nodes = dict(zip(self.active_nodes,
                [0 for x in self.active_nodes]))
        else:
            dict_act_nodes = dict(zip(self.active_nodes,
                [np.zeros(setup_mf.number_timesteps-1) for x in self.active_nodes])) 

        #tsm = setup_mf.setting_time if setup_mf.setting_time else 0
        iter_sh = lstfile_nwt_shortage(setup_mf)             
    
        #for k, tstep in enumerate(setup_mf.timesteps_enddates):
        for k, tstep_sh in enumerate(iter_sh):
            ### TODO: make this aware which time step is returned
            if tstep_sh[1][0] < setup_mf.setting_time:
                continue
            if tstep_sh:
                for i, well in enumerate(tstep_sh[0]):
                    
                    idx = reg_well.find_well_loc(well[:3])
                    
                    if idx:
                        pywr_node = reg_well[idx].pywr_node
                        
                        if not pywr_node in dict_act_nodes:
                            continue
                    
                        if not setup_mf.modePywr== "total":
                            dict_act_nodes[pywr_node][tstep_sh[1][0] - setup_mf.setting_time] = well[3]
                        else:
                            dict_act_nodes[pywr_node] = well[3]
        return dict_act_nodes


    @property
    def results(self):
        return self.__results


if __name__ == "__main__":
    pass

