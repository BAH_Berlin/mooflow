# -*- coding: utf-8 -*-
"""
The mooflow configuration.

@author: Ruben Müller
@email: ruben.mueller@bah-berlin.de
"""

import datetime
import sys
import os
import subprocess
import threading
from mooflow.utils import (doy, timestep_gen, class_by_bisect)


if sys.version_info >= (3, 3):
    from shutil import which
else:
    from distutils.spawn import find_executable as which

if sys.version_info > (3, 0):
    import queue as Queue
else:
    import Queue



class Omo_setup:
    def __init__(self, timestep):
        """Class for the mooflow setup.

        Parameters
        ----------
        timestep : int
            timestep is the temporal solution with which the model works"""
        self.timestep = timestep
        self.modelfolder = None
        self.executable = None
        self.inputfolder = None
        self.namefile = None
        self.modelname = None
        self.setting_time = [False, 0]
        self.__useperiods = False
        self.__timesteps = []
        self.__timesteps_d = []
        self.__timesteps_s = []
        self.__numper= 0
        self.__threadnumber = None
        

    ################ name file
    @property
    def namefile(self):
        return self.__namefile

    @namefile.setter
    def namefile(self, name=None):
        """Name of the nam file."""
        if name and not name[-4:].lower() == '.nam':
            name = name + '.nam'
        self.__namefile = name
        self.__modelname = name[:-4]


    # ################ modelname
    @property
    def modelname(self):
        return self.__modelname


    ################ starting date
    @property
    def starting_date(self):
        return self.__starting_date

    # TODO: check against setting time 
    @starting_date.setter
    def starting_date(self, stdate):
        """Starting date of the MODFLOW simulation, given as datetime.datetime."""
        if not isinstance(stdate, datetime.datetime):
            raise TypeError('Provide a datetime.datetime')
        self.__starting_date = stdate


    ################ setting time
    @property
    def setting_time(self):
        return self.__settingtime

    @setting_time.setter
    def setting_time(self, stime):
        """Setting time of the MODFLOW model. Discard stime time steps."""
        self.__settingtime = stime
        
    ################ setting time
    @property
    def threadnumber(self):
        return self.__threadnumber

    @threadnumber.setter
    def threadnumber(self, threadnumber):
        """The current thread id used. Maps against a model copy with ending __threadnumber."""
        self.__threadnumber = threadnumber

    ################ Pywr mode
    @property
    def modePywr(self):
        return self.__pywrmode

    @modePywr.setter
    def modePywr(self, mode="total"):
        """Time step the Pywr model uses. total is for a total balance."""
        if not mode.lower() in ("total", "monthly", "daily"):
            raise ValueError("Currently only total and monthly modes are implemented")
        self.__pywrmode = mode

    ################
    @property
    def timestep_lengths(self):
        return self.__timesteps
    
    @property
    def timesteps_end_doy(self):
        return self.__timesteps_s
    
    @property
    def timesteps_enddates(self):
        return self.__timesteps_d
    
    @property
    def number_timesteps(self):
        return self.__numper


    def read_disfile(self, disfile=None):
        """Read the discretisation file \*.dis.
        
        Parameters
        ----------
        disfile : str of None
            overwrite the filename for the dis file."""
        if not disfile:
            disfile = self.__modelname + '.dis'
        timesteps = []
        timesteps_s = []
        timesteps_dates = []
        dipcum_tmp = 0
        #dip = []
        with open(os.path.join(self.modelfolder, disfile), 'r') as fid:
            for i, line in enumerate(fid):
                ls = line.split()
                if i < 2:
                    try:
                        tmp = [int(k) for k in ls]
                        if not tmp[-2] == 4:
                            raise NotImplementedError('Sorry, works only for days yet...')
                        self.timestepmode = tmp[-2]
                        
                        if self.timestepmode == 1:
                            self.jump = lambda x: datetime.timedelta(seconds = x)
                        elif self.timestepmode == 2:
                            self.jump = lambda x: datetime.timedelta(minutes = x)
                        elif self.timestepmode == 3:
                            self.jump = lambda x: datetime.timedelta(hours = x)
                        elif self.timestepmode == 4:
                            self.jump = lambda x: datetime.timedelta(days = x)
                        elif self.timestepmode == 5:
                            NotImplementedError("Years are not supported yet.")
                        
                        num_periods = tmp[-3]
                    except:
                        pass

                if i > 3 and not ls[0].lower() == 'open/close':
                    days = float(ls[0])
                    dipcum_tmp += days
                    timesteps_s.append(dipcum_tmp)
                    timesteps.append(days)
                    timesteps_dates.append(self.starting_date + self.jump(dipcum_tmp))

        self.__timesteps = timesteps[:num_periods]
        self.__timesteps_s = timesteps_s[:num_periods]
        self.__timesteps_d = timesteps_dates[:num_periods]

        self.__ending_date = timesteps_dates[-1]
        self.__numper = num_periods


    def day_of_period(self, tstep):
        '''Look-up the period for the tstep and determine which day
        of the period it is.

        Parameters
        ----------
        tstep : int
            the time step

        Returns
        -------
        list : number of period, number of day in this period
        '''
        if isinstance(tstep, datetime.datetime):
            tstep = doy(tstep)
        else:
            tstep = doy(self.__timesteps_d[tstep])
        p = class_by_bisect(tstep, self.__timesteps_s, direction='right')
        pday = p if p == 0 else p - self.__timesteps_s[p-1]
        return p, pday


    ################ timestep
    @property
    def timestep(self):
        return self.__timestep

    @timestep.setter
    def timestep(self, timestep):
        """Basic timestep on that MODFLOW operates (day, week, month)"""
        if timestep in (1, 'day', 'Day'):
            self.__timestep = 1
        elif timestep in (7, 'week', 'Week'):
            raise NotImplementedError #self.__timestep = 7
        elif timestep in (12, 'month', 'Month'):
            self.__timestep = 12
        else:
            raise ValueError('timestep must be 1-day, got {}'.format(timestep))


    ################ modelfolder
    @property
    def modelfolder(self):
        return self.__modelfolder

    @modelfolder.setter
    def modelfolder(self, folder):
        """Set the model folder."""
        if folder == None:
            self.__modelfolder = folder
        else:
            if os.path.isdir(folder):
                self.__modelfolder = folder
            else:
                raise OSError('Model folder {} not found'.format(folder))


    ################ executable
    @property
    def executable(self):
        return self.__executable


    @executable.setter
    def executable(self, file):
        """The name of the MODFLOW executable."""
        if file == None:
            self.__inputfolder = None
            return

        if os.path.isabs(file):
            ifolder = file
        else:
            ifolder = os.path.join(self.__modelfolder, file)

        if os.path.isfile(ifolder):
            self.__executable = ifolder
        else:
            raise FileNotFoundError('Modflow executable {} not found in model folder {}'.format(file,
                                                                                                self.modelfolder))


    ################ input folder
    @property
    def inputfolder(self):
        return self.__inputfolder

    @inputfolder.setter
    def inputfolder(self, folder="ref"):
        """name of the subfolder with the input files, default is "ref" """
        if folder == None:
            self.__inputfolder = self.__modelfolder
            return

        if os.path.isabs(folder):
            ifolder = folder
        else:
            ifolder = os.path.join(self.__modelfolder, folder)

        if os.path.isdir(ifolder):
            self.__inputfolder = ifolder
        else:
            raise OSError('Input folder {} not found'.format(folder))


    def run_model(self,nsilent=False, report=False,
         normal_msg='normal termination', use_async=False, 
         cargs=None, silent=True):
        """This function will run the model using subprocess.Popen.  It
        communicates with the model's stdout asynchronously and reports
        progress to the screen with timestamps
        --> this is an adaption of the run_model function from the MODFLOW package.

        Parameters
        ----------
        silent : boolean
            Echo run information to screen (default is True).
        pause : boolean, optional
            Pause upon completion (default is False).
        report : boolean, optional
            Save stdout lines to a list (buff) which is returned
            by the method . (default is False).
        normal_msg : str
            Normal termination message used to determine if the
            run terminated normally. (default is 'normal termination')
        use_async : boolean
            asynchonously read model stdout and report with timestamps.  good for
            models that take long time to run.  not good for models that run
            really fast
        cargs : str or list of strings
            additional command line arguments to pass to the executable.
            Default is None

        Returns
        -------
        (success, buff)
        success : boolean
        buff : list of lines of stdout """

        success = False
        buff = []

        # convert normal_msg to lower case for comparison
        if isinstance(normal_msg, str):
            normal_msg = [normal_msg.lower()]
        elif isinstance(normal_msg, list):
            for idx, s in enumerate(normal_msg):
                normal_msg[idx] = s.lower()

        # Check to make sure that program and namefile exist
        exe = which(self.__executable)
        if exe is None:
            import platform
            if platform.system() in 'Windows':
                if not self.__executable.lower().endswith('.exe'):
                    exe = which(self.__executable + '.exe')
        if exe is None:
            s = 'The program {} does not exist or is not executable.'.format(
                self.__executable)
            raise Exception(s)
        else:
            if not silent:
                s = 'FloPy is using the following ' + \
                    ' executable to run the model: {}'.format(exe)
                print(s)

        if self.__namefile is not None:
            tmp = os.path.join(self.__modelfolder,
                               self.__namefile)
            if not os.path.isfile(tmp):
                s = 'The namefile for this model ' + \
                    'does not exist: <{}>'.format(tmp)
                raise Exception(s)

        # simple little function for the thread to target
        def q_output(output, q):
            for line in iter(output.readline, b''):
                q.put(line)

        # create a list of arguments to pass to Popen
        argv = [self.__executable]
        if self.__namefile is not None:
            argv.append(self.__namefile)

        # add additional arguments to Popen arguments
        if cargs is not None:
            if isinstance(cargs, str):
                cargs = [cargs]
            for t in cargs:
                argv.append(t)

        # run the model with Popen
        proc = subprocess.Popen(argv,
                        stdout=subprocess.PIPE,
                        stderr=subprocess.STDOUT,
                        cwd=self.__modelfolder)

        if not use_async:
            while True:
                line = proc.stdout.readline()
                c = line.decode('utf-8')
                if c != '':
                    for msg in normal_msg:
                        if msg in c.lower():
                            success = True
                            break
                    c = c.rstrip('\r\n')
                    if not silent:
                        print('{}'.format(c))
                    if report == True:
                        buff.append(c)
                else:
                    break
            return success, buff

        # some tricks for the async stdout reading
        q = Queue.Queue()
        thread = threading.Thread(target=q_output, args=(proc.stdout, q))
        thread.daemon = True
        thread.start()

        failed_words = ["fail", "error"]
        last = datetime.now()
        lastsec = 0.
        while True:
            try:
                line = q.get_nowait()
            except Queue.Empty:
                pass
            else:
                if line == '':
                    break
                line = line.decode().lower().strip()
                if line != '':
                    now = datetime.now()
                    dt = now - last
                    tsecs = dt.total_seconds() - lastsec
                    line = "(elapsed:{0})-->{1}".format(tsecs, line)
                    lastsec = tsecs + lastsec
                    buff.append(line)
                    if not silent:
                        print(line)
                    for fword in failed_words:
                        if fword in line:
                            success = False
                            break
            if proc.poll() is not None:
                break
        proc.wait()
        thread.join(timeout=1)
        buff.extend(proc.stdout.readlines())
        proc.stdout.close()

        for line in buff:
            if normal_msg in line:
                #print("success")
                success = True
                break

        return success, buff


    def get_ts_iterator_day(self):
        '''return an iterator too loop over each timestep in the simulation period'''
        return timestep_gen(self)


#%%

if __name__ == "__main__":
    if 0:
        loc = 3
        ### work or home...
        if loc == 1:
            mf = '/home/ruben/Work/mod1/'
            mf2 = '/home/ruben/Work/mod1/'
        elif loc == 2:
            mf = '/home/ruben/BAH/MedWater/mod1/'
            mf2 = '/home/ruben/bah_python/modflow_testing'
        else:
            mf = 'D:\Medwater\Model1'
            mf2 = 'S:\\bah_python\\modflow_testing'
    
        setup = Omo_setup(1)
        setup.starting_date = datetime.datetime(2009, 7, 1)
        setup.ending_date = datetime.datetime(2009, 7, 5)
        print(setup.starting_date)
        setup.modelfolder = mf
        print(setup.modelfolder)
        setup.inputfolder = 'ref'
        print(setup.inputfolder)
        setup.executable =  'mf2005.exe' #'mf2005' #
        print(setup.executable)
    
        tsi1 = timestep_gen(setup) #timestep_gen(setup.starting_date, setup.timestep, setup.timesteps)
        print(id(tsi1))
        for i in tsi1:
            print(i)
    
        tsi2 = timestep_gen(setup) #timestep_gen(setup.starting_date, setup.timestep, setup.timesteps)
        print(id(tsi2))
        for i in tsi2:
            print(i)

