#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Wellgroup class for grouping wells.

@author: Ruben Müller
@email: ruben.mueller@bah-berlin.de
"""

import collections
import numpy as np
from mooflow.classes import Well
from mooflow.params import check_boundary_len
from mooflow.params import check_lu_range



class Wellgroup:
    def __init__(self, idx):
        self.id = idx
        self.__well_list = []
        self.__pattern_ = None
        self.__l_bound = None
        self.__u_bound = None
        self.__num_par = 0
        
        
    @property
    def num_par(self):
        return self.__num_par
    
    @num_par.setter
    def num_par(self, par):
        """The number of optimization parameters for this well"""
        if isinstance(par, int):
            self.__num_par = max(par, 0) if not par == -1 else -1
        elif isinstance(par, collections.Iterable):
            non_nan = len(par) - np.sum(np.isnan(par))
            if non_nan == 1:
                self.__num_par = 1
            else:
                non_nan = len(par) - sum([1 for x in par if x == -999])
                if not non_nan:
                    print('all out -999 boundary vector detected for {}'.self.id)
                self.__num_par = non_nan


    def get_settings(self, setting):
        '''get overview from : lbound, ubound, len_pattern, wells'''
        if setting.lower() == "lbound":
            return self.__l_bound
        elif setting.lower() == "ubound":
            return self.__u_bound
        elif setting.lower() == "len_pattern":
            return self.__pattern_
        elif setting.lower() == "wells":
            return self.__well_list
        else:
            raise ValueError("wrong argument provided.")


    def idx_of_well(self, wellidx):
        '''return the index of the well in the list

        Parameters
        ----------
        wellidx : int or Well class
            the ID or the Well object to search for'''
        idxs = self.__well_id(wellidx)
        idx = [i for i, w in enumerate(self.__well_list) if w.id == idxs]
        return idx


    def __well_id(self, well):
        ''''check if the well argument is a Well object or a int.'''
        idxs = None
        if isinstance(well, int):
            idxs = well
        elif isinstance(well, Well):
            idxs = well.id
        return idxs


    def is_in_group(self, well):
        '''check if the well is in the list

        Parameters
        ----------
        wellidx : int or Well class
            the ID or the Well object to search for'''
        idxs = self.__well_id(well)
        check = any({w.id == idxs for w in self.__well_list})
        return check


    def add_wells(self, well):
        '''add a new well to the group IFF the ID is not already in the list

        Parameters
        ----------
        wellidx : int or Well class
            the ID or the Well object to search for'''
            
        if not isinstance(well, collections.Iterable):
            well = [well]
            
        for w in well:
            if not self.__well_list:
                self.__pattern_ = len(well[0].get_pattern_ext()[0])

            if self.is_in_group(w):
                index = self.idx_of_well(w)
                idxs = self.well_id(w)
                raise ValueError('well with id {} already in gallery, at indize {}'.format(idxs, 
                                 index))
            else:
                if not len(w.get_pattern_ext()[0]) == self.__pattern_:
                    raise ValueError('''Extraction pattern of well does not match to the group.
                                     Assign the well to a matching group. Well {} - Group {}'''.format(len(w.get_pattern_ext()[0]),
                                                                                                       self.__pattern_))
                w.well_group = self.id
                self.__well_list.append(w)
                w.__gallery = self.id


    def set_group_multiplier(self, multiplier):
        '''set the multiplier for all wells in the group

        Parameters
        ----------
        wellidx : int or Well class
            the ID or the Well object to search for'''
        for well in self.__well_list:
            well.multiplier = multiplier
            
            
    def set_sector(self, sector):
        '''set the sector for all wells in the group

        Parameters
        ----------
        wellidx : int or Well class
            the ID or the Well object to search for'''
        for well in self.__well_list:
            well.sector = sector
            
            
    def set_pywr_node(self, pywr_node):
        '''set the sector for all wells in the group

        Parameters
        ----------
        pywr_node : str
            Name of the node that the well provides water to'''  

        for well in self.__well_list:
            well.pywr_node = pywr_node


    def set_l_u_boundaries(self, l_bound, u_bound):
        '''set the multiplier for all wells in the group

        Parameters
        ----------
        wellidx : int or Well class
            the ID or the Well object to search for'''
        self.__l_bound = check_boundary_len(l_bound, self.__pattern_)
        self.__u_bound = check_boundary_len(u_bound, self.__pattern_)
        check_lu_range(self.__l_bound, self.__u_bound)
        
        
    def all_id_groups(self):
        """Returns all IDs of well groups"""
        return [w.id for w in self.__well_list]


    def all_groups(self):
        """Returns all IDs of well groups"""
        return [w for w in self.__well_list]
    


    def __getitem__(self, index):
        if isinstance(index, slice):
            return self.__well_list[index]
        elif isinstance(index, int):
            return self.__well_list[index]
        elif isinstance(str, str):
            return self.__well_list[self.idx_of_well(index)]


    def __len__(self):
        return len(self.__well_list)


    def __str__(self):
        return '{} well(s) in totel: {}'.format(len(self), self.__well_list)


    def __repr__(self):
        return 'Wellgroup(%r)' % self.id
    

if __name__ == "__main__":
    pass

