# -*- coding: utf-8 -*-
from .util_funcs import distance
from .util_funcs import closest_feasible
from .util_funcs import valid
