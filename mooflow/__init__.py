#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec  6 13:44:09 2018

@author: ruben
"""

__version__ = "0.7b1"



from mooflow import classes
from mooflow import utils
from mooflow import funcs
from mooflow import read
from mooflow import write
from mooflow import params
from mooflow import analyse
from mooflow import opt

from mooflow.moo_algos import create_mocmaes