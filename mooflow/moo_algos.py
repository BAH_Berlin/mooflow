# -*- coding: utf-8 -*-
"""
The class Mocmaes handles the MO-CMA-ES algorithm from DEAP.

Use the class factory **create_mocmaes**.

@author: Ruben Müller
@email: ruben.mueller@bah-berlin.de
"""
import os
import sys
import json
import shutil
import errno
import numpy as np
import multiprocessing
multiprocessing.freeze_support()  
import numpy


global __ext_fun
global __idx_queue_global


def _copy_recur_(src, dest):
    '''recursive copy of folders and files

    Parameters
    ----------
    src : str
        the source folder
    dest : str
        the destination folder'''
    try:
        shutil.copytree(src, dest)
    except OSError as e:
        # If the error was caused because the source wasn't a directory
        if e.errno == errno.ENOTDIR:
            shutil.copy(src, dest)


def initIndividual(icls, content):
    return icls(content)


def initPopulation(pcls, ind_init, filename):
    with open(filename, "r") as pop_file:
        contents = json.load(pop_file)
    return pcls(ind_init(c) for c in contents)


def create_mocmaes(config):
    """class factory"""
    return Mocmaes(config)


class Mocmaes:
    """Class to setup and run the optimization.
    
    Parameters
    ----------
    config : dict
        with the following fields:   
        
        "num_parameter" 
             number of optimimzation parameters;        
        "num_fitness"
             number of fitness functions;        
        "pop_size"
             size of the population (for MOCMAES not so big)        
        "fitness_weights"
             list with length num_fitness, -1 for minimization, 1 for maximization;         
        "target_folder":
            base folder in which the model folder resides;        
        "modelname"
            the name of the model folder        
        "lower_boundary" 
             lower box constraint (better not change), default 0;        
        "upper_boundary"
            upper box constraint (better not change), default 1;        
        "num_threads"
            number of threads to use. should exceed the number of physical cores, default: 2;        
        "init_population"
            set the parameters for the inidividuals of the initial
            population, not tested. numpy array with dimension pop x parameter, default False;        
        "seed"
            set the seed for the random number generator;
    """
    
    
    def __init__(self, config):
        self.conf = config
        self._chk_conf("lower_boundary", 0)
        self._chk_conf("upper_boundary", 1)
        self._chk_conf("pop_size", "error")
        self._chk_conf("num_parameter", "error")
        #self._chk_conf("eval_function", "error")
        self._chk_conf("num_threads", 2)
        self._chk_conf("num_fitness", "error")
        self._chk_conf("fitness_weights", "error")
        self._chk_conf("num_iters", 0)
        self._chk_conf("debug", False)
        self._chk_conf("seed", False)
        self._chk_conf("init_population", False)
        self._chk_conf("target_folder", "error")
        self._chk_conf("fitnessfile_with_path", "error")
        self._chk_conf("modelname", "error")
        #global __ext_fun
        #__ext_fun = config["eval_function"]
        self.DEBUG = config["debug"]
        self._write_moo_py()
        
        sys.path.insert(0, self.conf["target_folder"])
        
        
    def setup_modflow_parallel(self, new_basefolder=False):
        """Duplicate the modflow model for parallel evaluation.
        Each new modelfolder ends with __X, where X is the threadnumber
        from 0 to num_threads.
        
        Parameters
        ----------
        modelname : str
            the name of the modflow model folder
        new_basefolder : str
            overwrite the target_folder with this new folder, the model copies
            will be craeted in this folder."""
        src = os.path.join(self.conf["target_folder"], self.conf["modelname"])
    
        if new_basefolder:
            dest_base = os.path.join(new_basefolder, self.conf["modelname"])
            self.conf["target_folder"] = new_basefolder
        else:
            dest_base = src
        
        for thread in range(self.conf["num_threads"]):
            dest = dest_base + "__" + str(thread)
            if os.path.isdir(dest):
                print("model folder %s already exists, skipping copy" % dest)
                continue
            _copy_recur_(src, dest)


    def run(self):
        """run the multi-objective optimization with MOCMAES"""
        import mooflow_do_not_delete as moo5
        
        if self.conf['seed']:
            moo5.numpy.random.seed(self.conf['seed'])
        
        #print(self.p_dict['moo_init'] , type(self.p_dict['moo_init'] ))
        if isinstance(self.conf['init_population'], list):
            print('trying init')
            moo5.population = [moo5.creator.Individual(x) for x in self.conf['init_population']]
        else:
            moo5.population = [moo5.creator.Individual(x) for x \
                in np.random.uniform(0, 1, (self.conf["pop_size"], self.conf["num_parameter"]))]
        moo5.toolbox = moo5.base.Toolbox()
        
        if 0:
            moo5.toolbox.register("evaluate", moo5.registered_bench)
        else:
            moo5.toolbox.register("evaluate", moo5.registered_eval)
        
        moo5.strategy = moo5.cma.StrategyMultiObjective(moo5.population, sigma=0.4,
            mu=moo5.MU, lambda_=moo5.MU, 
            indicator=moo5.tools.additive_epsilon)
        moo5.toolbox.register("generate", moo5.strategy.generate, moo5.creator.Individual)
        moo5.toolbox.register("update", moo5.strategy.update)
        
        manager = multiprocessing.Manager()
        
        print('-----------------stage4')
        ### generate thread IDs and put them into the queue
        ids = [i for i in range(moo5.THREADS)]
        idQueue = manager.Queue()
        for i in ids:
            idQueue.put(i)
        
        print('-----------------stage5')
        ### open the multiprocessing pool with the queue
        pool =  multiprocessing.Pool(moo5.THREADS, moo5.initQ, (idQueue, ))
        
        print('-----------------stage6')
        ### tell the moo to use multiprocessing
        moo5.toolbox.register("map", pool.map)
        
        print('-----------------stage7')
        
        fitnesses = moo5.toolbox.map(moo5.toolbox.evaluate, moo5.population)
        
        ### here we only write the fitness values back
        for ind, fit in zip(moo5.population, fitnesses):
            ind.fitness.values = fit
        
        print('-----------------stage8----------------------------')
        ###############
        ### this is the loop for the iteration
        ###############
        for gen in range(moo5.NGEN):
            print('>>>>> generation {}'.format(gen))
            ### Generate a new population
            moo5.population = moo5.toolbox.generate()
                
            fitnesses = moo5.toolbox.map(moo5.toolbox.evaluate, moo5.population)
            for ind, fit in zip(moo5.population, fitnesses):
                ind.fitness.values = fit
        
            ### Update the strategy with the evaluated individuals
            moo5.toolbox.update(moo5.population)
        
            ### use the logbook to keep record of everything
            record = moo5.stats.compile(moo5.population) if moo5.stats is not None else {}
            moo5.logbook.record(gen=gen, nevals=len(moo5.population), **record)
        

    def _chk_conf(self, key, alt):
        if not key in self.conf:
            if alt == "error":
                raise KeyError("Key {} not specified in the configuration.".format(key))
            self.conf[key] = alt
            
            
    def _write_moo_py(self):
        """write the moo5.py into the target optimization folder"""
        with open(os.path.join(os.path.dirname(__file__), "_tmps_", "moo5.tmp"), "r") as fid:
            tmp_moo5 = fid.read()
        tmp_moo5 = tmp_moo5.replace('"XXXX"', repr(self.conf['fitness_weights']))
        tmp_moo5 = tmp_moo5.replace('THREADS', "THREADS = {}".format(self.conf['num_threads']))
        tmp_moo5 = tmp_moo5.replace('NGEN', "NGEN = {}".format(self.conf['num_iters']))
        tmp_moo5 = tmp_moo5.replace('NDIM', "NDIM = {}".format(self.conf['num_parameter']))    
        tmp_moo5 = tmp_moo5.replace('MU, LAMBDA', "MU, LAMBDA = {0}, {0}".format(self.conf['pop_size']))
        
        tmp_moo5 = tmp_moo5.replace('"XXX1"', repr(os.path.dirname(self.conf["fitnessfile_with_path"])))
        pyname = os.path.basename(self.conf["fitnessfile_with_path"]).rstrip(".py")
        
        tmp_moo5 = tmp_moo5.replace('"XXX2"', pyname)
        tmp_moo5 = tmp_moo5.replace('"XXX3"', pyname)
        
        with open(os.path.join(self.conf["target_folder"], "mooflow_do_not_delete.py"), "w") as fid: 
            fid.write(tmp_moo5)


if __name__ == "__main__":
    pass