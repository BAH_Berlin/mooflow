#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Functions to read the binary head file.

@author: Ruben Müller
@email: ruben.mueller@bah-berlin.de
"""

import os
import flopy.utils.binaryfile as bf
import numpy as np

def read_b_heads_file(setup_mf):
    """Read the heads file and returns a numpy array.

    Parameters
    ----------
    registry : mooflow.classes.Registry object
        the registry with the locations"""

    headfile = os.path.join(setup_mf.modelfolder, setup_mf.modelname +'.hds')
    try:
        headobj = bf.HeadFile(headfile)
    except FileNotFoundError:
        raise FileNotFoundError("Could not read the head file {}. Did the model run teminate correctly?".format(headfile))
    except OSError as e:
        raise e
    return headobj


def read_b_heads_reg(registry, setup_mf, timesteps=None, critical=False):
    """Read the heads for specified regions, timestep times and apply a function to integrate the heads

    Parameters
    ----------
    registry : mooflow.classes.Registry object
        the registry with the locations
    setup_mf : mooflow.classes.Omo_setup object
        the model setup
    timesteps : int or list of int, optional
        the timesteps to read. CAUTION: timesteps counting start with 0.
        if timesteps is None, then the heads for all timesteps of the simulation run
        (as provided by the setup_mf) are provided
        
    Returns
    -------
    head_sect : dict
        a dictionary with entries of region.ids and subentries of Z, if needed"""

    headobj = read_b_heads_file(setup_mf)
    if timesteps:
        timesteps = [timesteps] if not isinstance(timesteps, (tuple, list)) else timesteps
    else: 
        timesteps = list(range(setup_mf.timesteps[setup_mf.timestep]))
    head_sect = {}
    for i, region in enumerate(registry.registry):
        if not region.id in head_sect:
            head_sect.update({region.id: {'levels': [],
                                          'values': [],
                                          'weights': 1,
                                          'fitness_group': ''}})

        for z in region.z: 
            kij = [[z, x[0], x[1]] for x in region.area]
            #print(kij)
            try:
                ts = headobj.get_ts(kij)[timesteps, 1:]
            except Exception as e: #IndexError:
                if not critical:
                    ts = headobj.get_ts(kij)
                    shpe = np.shape(ts) 
                    timesteps = np.array(timesteps)
                    print('head file ran out of timesteps!', e)
                    ts = ts[(timesteps[np.argwhere(timesteps< shpe[0])].T)[0], 1:]
                else:
                    raise('head file ran out of timesteps!', e)                  
            #except Exception as e:
            #    raise(e)
            head_sect[region.id]['levels'].append(z)
            head_sect[region.id]['weights'] = region.weights
            head_sect[region.id]['fitness_group'] = region.fitness_group
            head_sect[region.id]['values'].append(region.apply_area_function1(ts))
    return head_sect


if __name__ == "__main__":
    pass
