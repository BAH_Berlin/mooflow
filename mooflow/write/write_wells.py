# -*- coding: utf-8 -*-
"""
Functions to write well files.

@author: Ruben Müller
@email: ruben.mueller@bah-berlin.de
"""

import os
import numpy as np
import datetime

from mooflow.classes import (Register, Omo_setup)
from mooflow.utils import (datetime_ymd, make_shape_two)


def write_well_day(setup_mf, well_reg):
    '''write the pattern with the well files on a daily basis.
    
    Parameters
    ----------
    setup_mf : mooflow.classes.Omo_setup
        the modflow setup
    well_reg : mooflow.classes.Registry for wells
        the registry for wells
    '''
    if not isinstance(setup_mf, Omo_setup):
        raise TypeError('registry must be of type mooflow.classes.Omo_setup. Got {}'.format(type(setup_mf)))
    if not isinstance(well_reg, Register):
        raise TypeError('well_reg must be of type mooflow.classes.Registry. Got {}'.format(type(well_reg)))
    
    iter_timestep = setup_mf.get_ts_iterator_day()
    
    for str_per in iter_timestep:
        ### read
        filename_well = 'WEL_' + make_shape_two(str_per[0], length=4) + '.dat'
        file_well = os.path.join(setup_mf.inputfolder, filename_well)
        try:
            well_array = np.loadtxt(file_well)        
        except:
            # if there is no file, try for other timesteps
            for x in range(str_per[0], -1, -1):  
                filename_well2 = 'WEL_' + make_shape_two(x, length=4) + '.dat'
                file_well2 = os.path.join(setup_mf.inputfolder, filename_well2)
                if os.path.isfile(file_well2):
                    well_array = np.loadtxt(file_well2)
                    break            

        ### replace
        for well in well_reg:
            idx_well = np.argwhere(np.all([well_array[:, 0] == well.location['z'], 
                                           well_array[:, 1] == well.location['y'], 
                                           well_array[:, 2] == well.location['x']], axis=0))
            idx_well = idx_well[0][0] if len(idx_well) else -1
            ### write only if acitve since <= current date
            if well.active_on[1] * 100000 + well.active_on[0] <= str_per[1][1] * 100000 + str_per[0]:
                extr_rate = well.get_rate_ext(*str_per[1])
                well_array[idx_well, -1] = extr_rate

        well_array = np.savetxt(file_well, well_array, delimiter='\t', fmt='%d  %d  %d  %10.3f')


def write_well_dinm(setup_mf, well_reg):
    '''write the pattern with the well files for each month of the year.
    
    Parameters
    ----------
    setup_mf : mooflow.classes.Omo_setup
        the modflow setup
    well_reg : mooflow.classes.Registry for wells
        the registry for wells
    '''
    if not isinstance(setup_mf, Omo_setup):
        raise TypeError('registry must be of type mooflow.classes.Omo_setup. Got {}'.format(type(setup_mf)))
    if not isinstance(well_reg, Register):
        raise TypeError('well_reg must be of type mooflow.classes.Registry. Got {}'.format(type(well_reg)))
    
    
    cumd = setup_mf.get_cum_per()[1:] #stressperiod_day(setup=setup_mf, extended=True)
    sday = datetime_ymd(setup_mf.starting_date)
    
    for i, str_per in enumerate(cumd):
        
        ### do not read/write dummy file for steady-state setting-time
        if i < setup_mf.setting_time:
            continue
        
        ### read
        filename_well = 'WEL_' + make_shape_two(i, length=4) + '.dat'
        file_well = os.path.join(setup_mf.inputfolder, filename_well)
        try:
            well_array = np.loadtxt(file_well)        
        except:
            # if there is no file, try for other timesteps
            for x in range(i, -1, -1):  
                filename_well2 = 'WEL_' + make_shape_two(x, length=4) + '.dat'
                file_well2 = os.path.join(setup_mf.inputfolder, filename_well2)
                if os.path.isfile(file_well2):
                    well_array = np.loadtxt(file_well2)
                    break           
                
        dday = sday + datetime.timedelta(days=max(str_per-1, 0))

        ### replace
        found = 0
        missing = []
        idx_seen = []
        for idx_well, well in enumerate(well_reg):
            
            if idx_well:
                idx_seen.append(idx_well)
                extr_rate = well.get_rate_ext(dday)
                well_array[idx_well, -1] = extr_rate
                found += 1
            else:
                missing.append(well.id)
        #print(found)
        well_array = np.savetxt(file_well, well_array, delimiter='\t', fmt='%d  %d  %d  %10.3f')

if __name__ == "__main__":
    pass