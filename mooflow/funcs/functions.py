# -*- coding: utf-8 -*-
"""
Functions to evaluations of drainages or ground water levels.

@author: Ruben Müller
@email: ruben.mueller@bah-berlin.de
"""

import numpy as np
from mooflow.classes import Area_gw



def linear_slope(x, plot_flag= False, begin=0):
    """Get the slope from a linear regression."""
    x = x[begin:, :]
    spe = np.shape(x)
    if len(spe) > 1:
        yf = np.reshape(x, (spe[0] * spe[1]), 'F')
        xf = np.tile(np.arange(0, spe[0]), spe[1]) #np.shape(x)[1])
    else:
        yf = x
        xf = np.arange(spe[0])
    try:
        if not len(np.unique(xf)) == 1 and not len(np.unique(yf)) == 1:
            m = np.polyfit(xf, yf, 1)
            
        else:
            m = [np.NaN]  
    except Exception as e:
        print(e)
        m = [np.NaN]
    return m[0]
    


def max_len_ones(inarray, indicator="max_len", parameter=14, fitness_group=None):
    """run length encoding. 
    If this function is set for a *set_area_function1*, then omit the inarray.
    
    Adopted from:
    https://stackoverflow.com/questions/1066758/find-length-of-sequences-of-identical-values-in-a-numpy-array-run-length-encodi
    
        
    Parameters
    ----------
    inarray : numpy array or list
        a 1d list or array with ones for violations and zeros for comliances with the
        gwr pattern
    indicator : str
        the type of evaluation: 
        *max_len*... the maximum consecuative days violating the constraint
        *num_days_th*... the longest period with consecuative violations over a treshold
        *perceptile*... returns a quantile of days with violation
    parameter : int
        the function of the parameter depends on the indicator
        *num_days_th*... number of allowed consecuative days with violations
        *perceptile*... the percentile   
    """
    curr_ind = ("max_len", "num_days_gt", "perceptile")
    if not indicator in curr_ind:
        raise ValueError("Please select an indicator from {}".format(curr_ind))
    iax = np.asarray(inarray)                  # force numpy
    n = len(iax)
    ia = iax
    # ia = np.array(shape(iax), dtype=bool)
    # ia = False
    # np.argwhere(iax < parameter)
    
    if n == 0: 
        return (None, None, None)
    else:
        y = np.array(ia[1:] != ia[:-1])     # pairwise unequal (string safe)
        i = np.append(np.where(y), n - 1)   # must include last element posi
        z = np.diff(np.append(-1, i))       # run lengths
        p = np.cumsum(np.append(0, z))[:-1] # positions
        ind = z[p > 0]
        
        if not len(ind):
            return 0
        if indicator == "max_len":
            ind = max(ind)
        elif indicator == "num_days_th":
            ind = sum([ind > parameter])
        elif indicator == "perceptile":
            ind = np.percentile(ind, parameter)
        #return(z, p, ia[i])
        if not fitness_group:
            return ind
        else: 
            return {'values': ind, 'fitness_group': fitness_group}


def days_violated(x, restrictions, setup_mf, start_with=None):
    """calculates the longest consecuate period with a violation of a restriction
    
    Parameters
    ----------
    x : numpy array
        time series with the groundwater levels
    restrictions : GW_level_pattern
        holds the grounwater level restrictions
    setup_mf : Omo_setup
        the optimization setup
    start_with : list
        no is done evaluation before [year, doy]
        
    Returns
    int : number of timesteps in the longest running violation period"""
    if isinstance(restrictions, Area_gw):
        p = np.zeros((np.shape(x)[0]))
        xq = np.percentile(x, 50, axis=1)
        ### use timestep iterator to get gw_pattern for doy
        if start_with:
            ### convert datetime into doy, year and year* 10000 + doy
            raise NotImplementedError
        iter_timestep = setup_mf.get_ts_iterator_day()
        for stress_p in iter_timestep:
            restriction = restrictions.get_level(*stress_p[1])
            p[stress_p[0]] = xq[stress_p[0]] < restriction
    else:
        p = np.array([sv < restrictions[i] for i, sv in enumerate(x)])
    return sum(p)


def sum_heads_fg(dict_heads, fitness_group):
    """calculates the weighted sum of all the indicators in the head dict
    for one fitness group.
    
    Parameters
    ----------
    dict_heads : dict
        the heads dict / heads evaluated with areal functions for locations
    fitness_group : str
        the desired fitness group
        
    Returns
    -------
    int : the weighted sum of the indicators"""
    value = 0
    for locd in dict_heads.items():
        if locd[1]['fitness_group'] == fitness_group:
            value += sum([a*b for a,b in zip(locd[1]['values'], locd[1]['weights'])])
    return value


def count_neg_slopes(dict_heads, fitness_group):
    """count the number of area_gw with negative slopes --> depletion trend
    
    Parameters
    ----------
    dict_heads : dict
        the heads dict / heads evaluated with areal functions for locations
    fitness_group : str
        the desired fitness group
        
    Returns
    -------
    int : the weighted sum of the indicators"""
    value = 0
    if fitness_group == 'all':
        for locd in dict_heads.items():
            k = [1 if locd[1]['values'][0] < 0 else 0]
            value += k[0]
    else:
        for locd in dict_heads.items():
                k = [1 if locd[1]['values'][0] < 0 and locd[1]['fitness_group'] == fitness_group else 0]
                value += k[0]
    return value   


def sum_neg_slopes(dict_heads, fitness_group, multiplier=1):
    """sum the negative slopes of selected area_gw, if we have an positive slope,
    then assign zero --> minimize the depletion trends, but do not recommend positive trends.
    
    Parameters
    ----------
    dict_heads : dict
        the heads dict / heads evaluated with areal functions for locations
    fitness_group : str
        the desired fitness group
    multiplier : float
        multiplies with the negative slopes.
    Returns
    -------
    int : the weighted sum of the indicators"""
    value = 0
    if fitness_group == 'all':
        for locd in dict_heads.items():
            k = [locd[1]['values'][0]*multiplier if locd[1]['values'][0] < 0 else 0]
            value += k[0]
    else:
        for locd in dict_heads.items():
                k = [locd[1]['values'][0]*multiplier if locd[1]['values'][0] < 0 and locd[1]['fitness_group'] == fitness_group else 0]
                value += k[0]
    return abs(value)   


if __name__ == "__main__":
    pass
