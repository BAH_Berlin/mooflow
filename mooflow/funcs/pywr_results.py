# -*- coding: utf-8 -*-
"""
Functions to valuate Pywr results.

@author: Ruben Müller
@email: ruben.mueller@bah-berlin.de
"""

def pywr_total__no_recorder_noparameters(pywr_model):
    """comput the deficits, provided rates for total balance runs (no time series, no input data time series written.)"""
    mgmt_nodes = {}
    mgmt_nodes_i = {}
    for node in pywr_model.jfile['nodes']:
        ### select demand nodes
        if node['type'] == 'output':
            mgmt_nodes.update({node['name']: {'demand': node['max_flow'],
                                 'cost': node['cost'],
                                 'deficit': 0,
                                 'deficit_percent': 0}})
        if not node['name'] in pywr_model.active_nodes and node["type"] == "input":
            mgmt_nodes.update({node['name']: {'source': node['max_flow'],
                                 'cost': node['cost'],
                                 "provided": 0,
                                 'provided_percent': 0}})

        if node['name'] in pywr_model.active_nodes and node["type"] == "input":
            mgmt_nodes_i.update({node['name']: {'source': node['max_flow'],
                                 'cost': node['cost'],
                                 "provided": 0,
                                 'provided_percent': 0}})
    for dnode in mgmt_nodes:
        if  "demand" in mgmt_nodes[dnode]:
            mgmt_nodes[dnode]['deficit'] = (mgmt_nodes[dnode]['demand'] \
                                           - pywr_model.df.loc[pywr_model.df.index[0], dnode])[0]
            mgmt_nodes[dnode]['demand'] = mgmt_nodes[dnode]['demand']
            if mgmt_nodes[dnode]['deficit'] > 0:
                mgmt_nodes[dnode]['deficit_percent'] = (mgmt_nodes[dnode]['deficit'] \
                                                       / mgmt_nodes[dnode]['demand']) * 100
        else:
            mgmt_nodes[dnode]['provided'] = pywr_model.df.loc[pywr_model.df.index[0], dnode][0]
            if mgmt_nodes[dnode]['provided'] > 0:
                mgmt_nodes[dnode]['provided_percent'] = (mgmt_nodes[dnode]['provided'] \
                                                        / mgmt_nodes[dnode]['source']) * 100
                
    for dnode in mgmt_nodes_i:
        mgmt_nodes_i[dnode]['provided'] = pywr_model.df.loc[pywr_model.df.index[0], dnode][0]
        if mgmt_nodes_i[dnode]['provided'] > 0:
            mgmt_nodes_i[dnode]['provided_percent'] = (mgmt_nodes_i[dnode]['provided'] \
                                                    / mgmt_nodes_i[dnode]['source']) * 100
    return mgmt_nodes, mgmt_nodes_i


if __name__ == "__main__":
    pass