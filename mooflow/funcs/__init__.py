# -*- coding: utf-8 -*-
"""
Created on Wed Dec 12 15:42:00 2018

@author: Ruben.Mueller
"""

from .functions import linear_slope
from .functions import days_violated
from .functions import max_len_ones
from .functions import sum_heads_fg
from .functions import count_neg_slopes
from .functions import sum_neg_slopes

from .pywr_results import pywr_total__no_recorder_noparameters