Background
==========

Mooflow was developed in the project `Medwater <https://bmbf-grow.de/en/joint-research-projects/medwater>`__.
Medwater is part of the funding measure Global Resource Water  "`GRoW <https://bmbf-grow.de/en>`__".


.. image:: _static/funding.png

Funding number 02WGR1428E B.


Author
======
Ruben Müller

Büro für Angewandte Hydrologie, Berlin

software@bah-berlin.de
		
.. image:: _static/bah-logo.png

.. toctree::
   :maxdepth: 2