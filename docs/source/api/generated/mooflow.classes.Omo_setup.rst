﻿mooflow.classes.Omo\_setup
==========================

.. currentmodule:: mooflow.classes

.. autoclass:: Omo_setup

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Omo_setup.__init__
      ~Omo_setup.day_of_period
      ~Omo_setup.get_ts_iterator_day
      ~Omo_setup.read_disfile
      ~Omo_setup.run_model
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~Omo_setup.executable
      ~Omo_setup.inputfolder
      ~Omo_setup.modePywr
      ~Omo_setup.modelfolder
      ~Omo_setup.modelname
      ~Omo_setup.namefile
      ~Omo_setup.number_timesteps
      ~Omo_setup.setting_time
      ~Omo_setup.starting_date
      ~Omo_setup.threadnumber
      ~Omo_setup.timestep
      ~Omo_setup.timestep_lengths
      ~Omo_setup.timesteps_end_doy
      ~Omo_setup.timesteps_enddates
   
   