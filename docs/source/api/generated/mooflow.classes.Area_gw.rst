﻿mooflow.classes.Area\_gw
========================

.. currentmodule:: mooflow.classes

.. autoclass:: Area_gw

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Area_gw.__init__
      ~Area_gw.apply_area_function1
      ~Area_gw.area_np_array
      ~Area_gw.get_level
      ~Area_gw.get_levels
      ~Area_gw.set_area_function1
      ~Area_gw.set_pattern
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~Area_gw.area
      ~Area_gw.fitness_group
      ~Area_gw.sector
      ~Area_gw.type
      ~Area_gw.weights
      ~Area_gw.z
   
   