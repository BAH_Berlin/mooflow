﻿mooflow.classes.Parameters
==========================

.. currentmodule:: mooflow.classes

.. autoclass:: Parameters

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Parameters.__init__
      ~Parameters.rescale_for_wellgroup
      ~Parameters.rescale_group
   
   

   
   
   