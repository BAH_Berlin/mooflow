﻿mooflow.classes.Extr\_pattern
=============================

.. currentmodule:: mooflow.classes

.. autoclass:: Extr_pattern

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Extr_pattern.__init__
      ~Extr_pattern.get_pattern_dem
      ~Extr_pattern.get_pattern_ext
      ~Extr_pattern.get_rate_dem
      ~Extr_pattern.get_rate_ext
      ~Extr_pattern.get_sum_year
      ~Extr_pattern.set_pattern
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~Extr_pattern.multiplier
   
   