Moo_algos
=========

.. currentmodule:: mooflow.moo_algos

Class to configure and start the MO-CMA-ES optimizer

.. autosummary::
   :toctree: generated/
   
   Mocmaes


Class factory for Mocmaes

.. autosummary::
   :toctree: generated/
   
   create_mocmaes

