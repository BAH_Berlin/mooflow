Opt
===

.. currentmodule:: mooflow.opt


Helper functions for the optimizer

.. autosummary::
   :toctree: generated/

   distance
   closest_feasible
   valid
