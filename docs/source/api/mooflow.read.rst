Read
====

.. currentmodule:: mooflow.read


Read MODLFOW model output

.. autosummary::
   :toctree: generated/

   read_well_file
   wells_and_wellgroups
   boundaries
   read_b_heads_reg
   lstfile_iter_well_drn
   lstfile_iter_balance
   lstfile_nwt_shortage
