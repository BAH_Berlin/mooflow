Util
====

.. currentmodule:: mooflow.utils


Functions for dates

.. autosummary::
   :toctree: generated/

   datetime_ymd
   dy_datetime
   is_leap_year
   doy
   woy
   ymd
   full_steps_timediff
   timestep_gen
   days_dt
   days_in_month
   stressperiod_day


Utility functions

.. autosummary::
   :toctree: generated/
   
   class_by_bisect
   make_shape_two
   load_obj_compressed
   save_obj
   load_obj
