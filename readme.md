# The mooflow package

Mooflow helps you to setup and perform multi-objective simulation-based optimization of ground water resources management. Mooflow couples the groundwater model [MODFLOW](https://www.usgs.gov/mission-areas/water-resources/science/modflow-and-related-programs)
with the water distribution network model [Pywr](https://github.com/pywr/pywr) and optimization algorithms from the [DEAP](https://github.com/DEAP/deap) package.

![Download](docs/source/_static/MKSBO_eng.png)
![MODLFOW-Pywr](docs/source/_static/patterns.png)

## Documentation

More documentation can be found at [readthedocs.io](https://mooflow.readthedocs.io).

## Example
The following code sniplets give a quick example how to use mooflow. For an optimization two scripts need to be prepared.

### Script 1
The main script configures and duplicates the optimization environment, then runs the algorithm.

```
import mooflow
from eval_modflow import eval_modflow
configuration = {
    "num_parameter": 10,			# number of optimization parameters
    "eval_function": eval_modflow,	# number of optimization parameters
    "num_threads": 12,				# parallization using 12 threads
    "fitness_weights": [-1, -1],	# -1 to minimize a fitness function
    "fitnessfile_with_path": "d:/modflow_model/eval_modflow.py"		# script with the evaluation function
    }

opt_mocmaes = mooflow.create_mocmaes(configuration)	# initialize the optimization algorithm
mocmaes.setup_modflow_parallel(configuration)	# duplicate the model for parallelization
mocmaes.run()	# run the optimization
```
### Script 2
Create a python script with the function *eval_modflow* which configures MODFLOW, executes MODFLOW, reads the results and calculates the fitness functions. The name of the function and the Python file must match.

```
import mooflow as mfl
def eval_modflow(parameters, threadnumber)
    **code here**
    return [fitness_0, fitness_1, fitness_n]
```

*parameters* are the optimization parameters given by the executing optimization algorithm, *threadnumber* is the id of the thread id that is linked to the evaluation (and the model copy)

The \*\**code here*\*\* section above covers all necessary functionality to configure and run MODFLOW and Pywr and calculate the fitness functions. It looks like this (not working example):

```
from datetime import datetime
import mooflow as mfl
# create an setup instance. the model operates on a daily timestep
setup_mf = mfl.classes.Omo_setup('day')
setup_mf.usePywr = True
setup_mf.starting_date = datetime(1987, 9, 15)
# … skipping other attributes to set...

# handle the incoming parameters
parameter = mfl.classes.Parameters(parameters) 

############
# wells and wellgroups
############
# 1 read configuration files (csv) for all wells in the model
# 2 assign predefined seasonal extraction patterns
# 3 group the wells into wellgroups to share parameters
# create and return registries for wells and wellgroups
reg_wellgroup, reg_well = mfl.read.wells_and_wellgroups(setup_mf, 
        wellgroupfile, patternfile, delimiter_wg="\t", delimiter_pf="\t",
        factor=1)

###########
# ground water level sensitive areas
############
# create a registry for ground water level sensitive areas
# create regions, which can span multiple grid points
# assign an evaluation function, e.g. days under threshold
# then add to registry
reg_argw = mfl.classes.Register('Area_gw')
for i, j in enumerate(well_locations): 
    locgw = mfl.classes.Area_gw('loc' + str(i), j)
    loc.set_area_function1(mfl.funcs.days_violated, 50, axis=1) 
    reg_argw.add(loc)

# we rescale the extraction patterns according to the optimization parameter
# then write the modified well files into a model
for i, wg in enumerate(reg_wellgroup):
    Parameter.rescale_for_wellgroup(wg)
mfl.write.write_well_dinm(setup_mf, reg_well)

############
# let's run MODFLOW
############
setup_mf.run_model(silent=True)

############
# time to evaluate
############
# read the heads for the ground water level  sensitive areas
dict_heads = mfl.read.read_b_heads_reg(reg_argw, setup_mf)
# read the list file
dict_lst = mfl.analyse.list_file(setup_mf, reg_well)
# prepare the Pywr model
# read the pumped rates (with the NWT model, look also for shortages) 
# and add the rates to the matching Pywr inflow nodes (active nodes)
dict_node_provides = Pywr_model.pywt_extr_nwt(setup_mf, 
            reg_well, reg_wellgroup)

# now write the inflow time series for the active nodes and run the Pywr model
Pywr_model.write_nodefile(setup_mf)
Pywr_model.run_pywr()

# finally get the results using an evaluation function
pywr_res = Pywr_model.read_results(mfl.funcs.pywr_total__no_recorder_noparameters)

############
# compute the fitness function
############
# for drainage points, e.g. look for the longest period with flow rate less then 70000
ind_drain = mfl.funcs.max_len_ones(dict_lst['drains array'] < 70000, 
                                           indicator="max_len")
# look for negative trends in the gw_areas and sum them up
ind_gw1 = mfl.funcs.sum_neg_slopes(dict_heads, 'slopes', multiplier=1000)
# evaluating the Pywr model is too case specific, the dictionary is optimally 
# analysed with an own function, e.g. here we sum up the shortages and 
# calculate costs of water provided by other sources in the distribution system
ind_shortages = get_shortages(pywr_res)
ind_costs = get_costs(pywr_res)

# now return the fitness functions, you can combine indicators to keep the optimization simple
return ind_drain + ind_gw1, ind_shortages, ind_costs
```

### Files for the relation between wells, well groups and extraction patterns, etc.

#### well file
The well file specifies several properties of every well with its identifier (**Index**). The location in the MODLOW model is given by **layer**, **row** and **column**. The generic extration or infiltration pattern *p_class* is defined in the [extraction pattern file](#extraction-pattern-
file) . As the extraction patterns are scaled between 0.0 and 1.0, the true range for the specific well is recalculated with the **min** and **max** values. Finally, the membership of the well in a **well_goup** enables the sharing of optimization parameters (scaling factors) and  **active_node** adds the extractions to an input node in the Pywr model

```
Index; layer; row; column; p_class; max;    min;      active_node;  well_group
w1;     1;    244;   23;      0;  -275.406; -2496.742;  east_gw;     wgroup_01
w2;     1;    265;   45;      0;  -275.406; -2496.742;  east_gw;     wgroup_01
w3;     2;    768;   676;     1;  -275.406; -2496.742;  south_gw;    wgroup_02
w4;     2;    888;   678;     2;  -275.406; -2496.742;  south_gw;    wgroup_02
...
```

   * *Index*… well names 
   * *layer*… the layer (z) of the well
   * *row*… the row (y) of the well
   * *column*… the column(x) of the well
   * *p_class*… the extraction pattern, e.g 0 maps to p_class_0 in the extraction pattern file
   * *max*… min and max are used to rescale the extraction pattern to its native range (which is then scaled using the optimization parameter)
   * *min*… see *max*
   * *active_node*… the extractions of this well are added to this Pywr input node
   * *well_group*… the respective well group (share parameters) 

#### extraction pattern file

All extractions patterns given in the extraction pattern file should be generic, so that they can be used for several wells. Therefore extraction pattern are scaled to the range between 0.0 and 1.0, and inividual patterns for wells can be recalculated by **min** and **max** in the [well file](#well-file).

```
Index; p_class_0; p_class_1;...
1	; 0.2	;	0.0
2	; 0.5	;	0.0
...	; ..	;	...
12	; 0.2	;	1.0
```

The extraction patterns are given for each season as end of season values. For monthly (30day) and weekly (7day) and daily time steps, the day of year is given at the single well level (see initialization of the Well class). This way, wells can have different season length and the user takes further care of grouping them.

#### boundary file

For optimization the extraction patterns are scaled up or down for the complete pattern or single season. 
How much a pattern can be scaled up or down for a specific well_goup is defined with the boundary file.  
For a model with 12 seasons the boundary file may look like this:

```
wellgroup; boundary; 1  ;   2; 3; 4; 5; 6  ; 7  ; 8  ; 9; 10; 11; 12
wgroup_01; lower;    0.5;    ;  ;  ;  ;    ;    ;    ;  ;   ;   ;  
wgroup_01; upper;    2.0;    ;  ;  ;  ;    ;    ;    ;  ;   ;   ;   
wgroup_02; lower;       ;    ;  ;  ;  ; 0.7; 0.8; 0.4;  ;   ;   ;
wgroup_02; upper;       ;    ;  ;  ;  ; 1.3; 1.8; 1.4;  ;   ;   ;
...
```

Optimizing the extraction patterns means scaling the complete pattern up or down or use different scaling factors for single seasons.
The extraction pattern for wgroup_01 can be halved or doubled (from 0.5 to 2.0).
For all wells group wgroup_02 only the season 5 to 7 are subject to optimization, 
all other season are not subject to change.

## Requirements
   * flopy
   * numpy
   * pandas
   * deap
   * pywr
   
## Current shortcomings
   * Pywr only accepts time series with a specific frequency
      * for example, monthly time series do not work (because 28, 29, 30 and 31 day months), here you need to convert to fixed 30day periods in a 360 day year or work with a daily resolution (complicated by pattern in demands, ...)
   * ground water infiltration can be optimized as part of an more complex water resources system. But the injection is realized in the first step when running the MODFLOW model. therefore you have to assure, that the rates are available in the pywr model. Otherwise mass balance is violated. If you can't save guard, then an error and must be raised or an high penalty must be added to all fitness function, so that the solution is rejected.
      * optimally, the penalty is a function of the violation, so that the algorithm can learn to learn to avoid, if possible.

## Author
Ruben Müller  
Büro für Angewandte Hydrologie, Berlin  
ruben.mueller@bah-berlin.de  
[webpage](www.bah-berlin.de)

![BAH](docs/source/_static/bah-logo.png)

## License
Copyright (C) 2020 Ruben Müller, Büro für Angewandte Hydrologie, Berlin  

This program is free software; you can redistribute it and/or modify  
it under the terms of the GNU General Public License as published by  
the Free Software Foundation; either version 1, or (at your option)  
any later version.

This program is distributed in the hope that it will be useful,  
but WITHOUT ANY WARRANTY; without even the implied warranty of  
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
GNU General Public License for more details.  

You should have received a copy of the GNU General Public License  
along with this program; if not, write to the Free Software  
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA  02110-1301 USA.

